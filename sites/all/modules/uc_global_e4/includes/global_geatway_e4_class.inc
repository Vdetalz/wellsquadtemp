<?php
class GlobalGeatwayE4Class {

    /**
     * @var string The Global Geatway E4 params to be used for requests.
     */

    private $gateway_id;
    private $password;
    private $key;
    private $key_id;

    public $url;
    public $transaction_id;


    public function setUrl($url) {
        $this->url = $url;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setKey($key) {
        $this->key = $key;
    }

    public function setKeyId($key_id) {
        $this->key_id = $key_id;
    }

    public function setGatewayId($gateway_id) {
        $this->gateway_id = $gateway_id;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function postTransaction($args = array()) {

        $ch = curl_init ();
			
        curl_setopt ($ch, CURLOPT_URL, $this->url);
        curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->getPayload($args));
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_VERBOSE, 1);

        $content_digest = sha1($this->getPayload($args));

        //$current_time = gmdate('Y-m-dTH:i:s') . 'Z';
        //$current_time = str_replace('GMT', 'T', $current_time);
		

		$current_time = date('Y-m-d...H:i:s') . 'Z';
	    $current_time = str_replace('...', 'T', $current_time);
        
		$code_string = "POST\napplication/json\n{$content_digest}\n{$current_time}\n/transaction/v14";
        $code = base64_encode(hash_hmac('sha1', $code_string, $this->key, true));

        $header_array = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($this->getPayload($args)),
            'X-GGe4-Content-SHA1: '. $content_digest,
            'X-GGe4-Date: ' . $current_time,
            'Authorization: GGE4_API ' . $this->key_id . ':' . $code,
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
        $result = curl_exec ($ch);

        curl_close($ch);
		
        return $result;
    }

    public function getPayload($args = array())
    {
        $args = array_merge(array(
            'gateway_id' => "",
            'password' => "",
            'transaction_type' => "",
            'amount' => "",
            'cardholder_name' => "",
            'cc_number' => "",
            'cc_expiry' => "",
            'cvd_code' => "",
            'client_ip' => "",
            'client_email' => "",
            'zip_code' => "",
            'address' => array(
                'address1' => "",
                'address2' => "",
                'city' => "",
                'state' => "",
                'zip' => ""
            ),

            ), $args);

        $args['gateway_id'] = $this->gateway_id;
        $args['password'] = $this->password;

        ksort($args);

        return drupal_json_encode($args);
    }

    public function getErrorMessages($response = NULL, $request_state = array())
    {
        $messages = array();
        if (isset($response)) {
            if (empty($response['transaction_approved'])) {
                if (!empty($response['bank_message'])) {
                    $messages['bank_message'] = $response['bank_message'];
                }

                if (!empty($response['avs']) && $response['avs'] != 'M') {
                    $messages['avs'] = t('AVS response:') . ' ' . self::avsMessage($response['avs']);
                }

                // Add the CVV response if enabled.
                if (!empty($response['cvv2'])) {
                    $messages['cvv2'] = t('CVV match:') . ' ' . self::cvvMessage($response['cvv2']);
                }
            }
        }

        $validation_errors = array();
        if (!empty($response['validation_errors'])) {
            $validation_errors = $response['validation_errors'];
        }
        elseif (!empty($request_state['validation_errors'])) {
            $validation_errors = $request_state['validation_errors'];
        }

        if (!empty($validation_errors)) {
            $i = 0;
            foreach ($validation_errors as $validation_error) {
                $messages['validation_' .  $i] = $validation_error;
                $i++;
            }
        }

        return $messages;
    }

    public static function avsMessage($code) {
        switch ($code) {
            case 'X':
                return t('Address (Street) and nine digit ZIP match');
            case 'Y':
                return t('Address (Street) and five digit ZIP match');
            case 'A':
                return t('Address (Street) matches, ZIP does not');
            case 'W':
                return t('Nine digit ZIP matches, Address (Street) does not');
            case 'Z':
                return t('Five digit ZIP matches, Address (Street) does not');
            case 'N':
                return t('No Match on Address (Street) or ZIP');
            case 'U':
                return t('Address information is unavailable');
            case 'G':
                return t('Non-U.S. Card Issuing Bank');
            case 'R':
                return t('Retry Р System unavailable or timed out');
            case 'E':
                return t('AVS error'); // Not a mail or phone order
            case 'S':
                return t('Service not supported by issuer');
            case 'Q':
                return t('Bill to address did not pass edit checks');
            case 'D':
                return t('International street address and postal code match');
            case 'B':
                return t('International street address match, postal code not verified due to incompatible formats');
            case 'C':
                return t('International street address and postal code not verified due to incompatible formats');
            case 'P':
                return t('International postal code match, street address not verified due to incompatible format');
            case '1':
                return t('Cardholder name matches');
            case '2':
                return t('Cardholder name, billing address, and postal code match');
            case '3':
                return t('Cardholder name and billing postal code match');
            case '4':
                return t('Cardholder name and billing address match');
            case '5':
                return t('Cardholder name incorrect, billing address and postal code match');
            case '6':
                return t('Cardholder name incorrect, billing postal code matches');
            case '7':
                return t('Cardholder name incorrect, billing address matches');
            case '8':
                return t('Cardholder name, billing address, and postal code are all incorrect');
        }

        return '-';
    }


    /**
     * Returns the message text for a CVV match.
     */
    public static function cvvMessage($code) {
        switch ($code) {
            case 'M':
                return t('Match');
            case 'N':
                return t('No Match');
            case 'P':
                return t('Not Processed');
            case 'S':
                return t('Should have been present');
            case 'U':
                return t('Issuer unable to process request');
        }

        return '-';
    }

}

The Publisher Desk helps grow and monetize your audience without the intricacy
 and cost of in-house development.

The Publisher Desk for Drupal module seamlessly integrates the Publisher Desk
 JavaScript library, targeting variables, and iframe busters to your exising
 Drupal site.

For more information about The Publisher Desk, please
 visit http://www.publisherdesk.com/

Installation:
  Expand module download in sites/all/modules/ directory.
  Enable module through Administration -> Modules (/admin/modules).
  Enter Publisher ID and other settings in Administration -> Configuration ->
   The Publisher Desk (/admin/config/content/publisherdesk).

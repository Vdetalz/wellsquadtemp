(function($) {
    $(document).ready(function() {
        $.colorbox({
            html: "<div class='thank-you-popup'>" +
                    "<h3>THANKS FOR SUBMITTING a<br /><span class='accent--colored'><span class='accent--coloredmin'>Squad Leader Profile!</span></span></h3>" +
                    "<div class='welcome-preview'><img alt='welcome preview' src='/sites/default/files/styles/large/public/images/img-1.png'></div>" +
                    "<h5>" +
                        "SQUAD LEADER PROFILE WILL HAVE TO BE APPROVED BY WELLSQUAD BEFORE PROFILE IS COMPLETE" +
                     "</h5>" +
                  "</div>"
        });
    })
})(jQuery)
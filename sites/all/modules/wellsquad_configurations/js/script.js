(function ($) {
    Drupal.behaviors.ajax_squad = {
        attach:function (context) {
            $(".squod-block").click (function() {
                var squadId = $(this).parent().attr('id').split('-');
                if(squadId.length == 0){
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: Drupal.settings.basePath+'ajax/squad',
                    data: {
                        squad_id: squadId[1]
                    },
                    success: function(data) {
                        $('.content', "#block-wellsquad_configurations-squad_users").html(data);

                        $('.users-wrapper').slick({
                            dots: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            responsive: [
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 3,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 2
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ]
                        });

                        var count = users_per_page('.slick-active');
                        if(count > 0){
                            $('.per-page-number').text(count);
                        }

                        $('.users-wrapper').on('afterChange', function(event, slick, currentSlide, nextSlide){
                            var count = users_per_page('.slick-active');
                            if(count > 0){
                                $('.per-page-number').text(count);
                            }

                        });
                    }
                });
            });
        }
    }


    Drupal.behaviors.change_message_status = {
        attach: function(context) {
            $('.messages').on('click', 'li.unread', function() {
                var element = $(this);

                $.ajax({
                    type: 'POST',
                    url: Drupal.settings.basePath + 'ajax/change-message-status',
                    data: {
                        mid: element.data('message-id')
                    },
                    success: function(messages_count) {
                        element.removeClass('unread');
                        if(messages_count > 0) {
                            $('.leaf.messages a span').html(messages_count);
                        } else {
                            $('.leaf.messages a span').remove();
                        }
                    }
                });
            })
        }
    }

    Drupal.behaviors.button_text = {
        attach: function(context) {

            setTimeout(function() {
				if($('#edit-uc-product-image .jcf-button-content', context).length){
					$('#edit-uc-product-image .jcf-button-content', context).text(Drupal.t('Select Photo'));
				}else{
					$('.jcf-button-content', context).text(Drupal.t('Upload File'));	
				}
                
            }, 200);
			
        }
    }

    Drupal.behaviors.squad_users = {
        attach: function(context) {
            var countSection = $('.per-page-number');
            //var count = users_per_page('.user');
            if(countSection.length){
                  setTimeout(function(){
                    $('.squads-carousel', context).each(function(){
                      var count = '';
                      $('.slick-active', this).each(function(){
                          if($(this).attr('data-number') > 0){
                              count = $(this).attr('data-number');
                          }
                      });

                      if(count > 0){
                          $('.per-page-number', this).text(count);
                      }
                  });
              }, 1000);
            }
        }
    }

    Drupal.behaviors.colorbox = {
        attach: function(context) {
            if($.colorbox){
                $('a', '#colorbox').click( function(){
                    $(window).colorbox.close();
                });
            }
        }
    }

    Drupal.behaviors.autoUpload = {
        attach: function(context, settings) {

            $('.form-item input.form-submit[value=Upload]', context).hide();

            setTimeout(function() {
                $( ".jcf-fake-input", context).not("#privatemsg-new .jcf-fake-input").hide();
                var buttonText = Drupal.t('UPLOAD PHOTO');
                $( ".profile-photo-wrap .jcf-button-content", context).text(buttonText);

                if($('#privatemsg-new').length){
                    var msgButtonText = Drupal.t('Attach file');
                    $("#privatemsg-new .jcf-button-content").text(msgButtonText);
                }
            }, 100);

            $('.form-item input.form-file', context).change(function() {
                $parent = $(this).closest('.form-item');

                //setTimeout to allow for validation
                setTimeout(function() {
                    $('input.form-submit[value=Upload]', $parent).mousedown();
                }, 100);
            });
        }
    };

	//need for mobile device
    Drupal.behaviors.regiser = {
        attach: function(context, settings) {
            $( document ).on( "click", "#popup-register", function( event ) {
               $.colorbox.close();
               $( ".ajax-register-links .first a" ).trigger( "click" );
                return false;

            });
        }
    };

    function users_per_page(searchClass){
        var count = 0;
        var countSection = $('.per-page-number');

        if(!countSection.length){
            return count;
        }

        var activeUsers = $('.profile-section--users').find(searchClass);
        activeUsers.each(function(index){
            if($(this).attr('data-number') > 0){
                count = $(this).attr('data-number');
            }
        });

        return count;
    }

})(jQuery);
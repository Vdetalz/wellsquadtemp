<?php
/**
 * Wellsquad store configuration
 * @return mixed
 */
function percent_configuration_page() {
    $form = array();

    $form['percent_for_squad'] = array(
        '#type' => 'textfield',
        '#title' => t('Amount of percent'),
        '#default_value' => variable_get('percent_for_squad', 0),
        '#description' => t("Amount of percent which squad leaders will receive. Ex. 30"),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}
<?php
    if(!empty($squads_users)){
    // CTools modal window
        ctools_include('modal');
        ctools_include('ajax');
        ctools_modal_add_js();
        drupal_add_js(array(
                'CToolsModal' => array(
                    'modalSize' => array(
                        'type' => 'fixed',
                        'width' => 450,
                        'height' => 350,
                        'contentBottom' => 800),
                ),
            ), 'setting');
    }
?>
<?php if(!empty($user_info)): ?>
    <section class="profile-section profile-section2 border-bottom profile-section-new">
        <h2 class="heading--styled profile-heading">
            <?php print t('Squad Leader'); ?>
        </h2>
        <div class="profile-info">
            <div class="profile-cart">
                <div class="photo-wrap">
                    <div class="profile-photo">
                        <?php print render($user_info->photo); ?>
                    </div>
                </div>
                <section class="param-holder">
                    <div class="param-info">
                        <h3><?php print render($user_info->name); ?></h3>
                        <?php if(!empty($user_info->gender)):?>
                            <div class="gender param-row">
                                <span><?php print t('Gender: ');?></span><?php print render($user_info->gender); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($user_info->age)):?>
                            <div class="age param-row">
                                <span><?php print t('Age: ');?></span><?php print render($user_info->age); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($user_info->location)):?>
                            <div class="location param-row">
                                <span><?php print t('Location: ');?></span><?php print render($user_info->location); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($user_info->profession)): ?>
                            <div class="profession param-row">
                                <span><?php print t('Profession: ');?></span><?php print render($user_info->profession); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($user_info->experience)):?>
                            <div class="experience param-row">
                                <span><?php print t('Years of Experience: ');?></span><?php print render($user_info->experience); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="buttons-section">
                        <?php print l(t('Add new squad'), 'create-squad', array('attributes' => array('class' => 'btn'))); ?>
                        <?php if(!empty($squads)): ?>
                            <?php print l(t('Send group message'), 'modal/nojs/new_group_message', array('attributes' => array('class' => 'btn ctools-use-modal ctools-modal-modal-popup-medium'))); ?>
                        <?php endif; ?>
                    </div>
                </section>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="profile-section bg--dark profile-section-squad">
	<div class="squads-wrapper-profile">
        <?php if(!empty($squads)):?>
            <?php foreach($squads as $nid => $squad): ?>
                <div id="squad-<?php print $nid; ?>" class="squod-item">
                    <div class="squod-block border-bottom">
                        <?php if(!empty($squad->image)): ?>
                            <div class="squod-preview border-right">
                                <div class=""><?php print render($squad->image); ?></div>
                            </div>
                        <?php endif; ?>
                        <div class="squod-description">
                            <div class="squod-about border-bottom">
                                <h3 class="squod-title">
                                    <?php print $squad->title; ?>
                                    <?php if(!empty($squad->user_number)): ?>
                                        <span>( <?php print $squad->user_number; ?><?php print t(' users'); ?> )</span>
                                    <?php endif; ?>
                                </h3>
                                <ul class="description-list">
<!--                                    --><?php //if(!empty($squad->tagline)): ?>
<!--                                    <li>-->
<!--                                        <h5 class="squod-tagline">--><?php //print $squad->tagline; ?><!--</h5>-->
<!--                                    </li>-->
<!--                                    --><?php //endif; ?>
                                    <?php if(!empty($squad->description)): ?>
                                        <li>
                                            <h5><?php print t('Description: ');?></h5>
                                            <div class="squod-description"><?php print $squad->description; ?></div>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->length)): ?>
                                    <li>
                                        <div class="squod-length">
                                            <h5><?php print t('Length of time: ');?></h5>
                                            <?php print $squad->length; ?>
                                        </div>
                                    </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->field_equipment)): ?>
                                        <li>
                                            <h5><?php print t('Equipment: ');?></h5>
                                            <div class="squod-equipment"><?php print render($squad->field_equipment); ?></div>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->field_squad_goals)): ?>
                                        <li>
                                            <h5><?php print t('Goals: ');?></h5>
                                            <div class="squod-goals"><?php print $squad->field_squad_goals; ?></div>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->program_start)): ?>
                                        <li>
                                            <h5><?php print t('Program Start: ');?></h5>
                                            <div class="squod-schedule"><?php print $squad->program_start; ?></div>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->program_end)): ?>
                                        <li>
                                            <h5><?php print t('Program End: ');?></h5>
                                            <div class="squod-schedule"><?php print $squad->program_end; ?></div>
                                        </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->schedule)): ?>
                                    <li>
                                        <h5><?php print t('Schedule: ');?></h5>
                                        <div class="squod-schedule"><?php print $squad->schedule; ?></div>
                                    </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->squad_offers)): ?>
                                    <li>
                                        <h5><?php print t('Squad Offers: ');?></h5>
                                        <div class="squod-squad-offers"><?php print $squad->squad_offers; ?></div>
                                    </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->location)): ?>
                                    <li>
                                        <h5><?php print t('Location: ');?></h5>
                                        <div class="squod-location"><?php print $squad->location; ?></div>
                                    </li>
                                    <?php endif; ?>

                                    <?php if(!empty($squad->openings)): ?>
                                    <li>
                                        <h5><?php print t('Openings: ');?></h5>
                                        <div class="squod-openings"><?php print $squad->openings; ?></div>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="squad-bottom">
                                <?php if(!empty($squad->price)): ?>
                                        <h5><?php print t('Price per month: ');?></h5>
                                        <div class="squod-price"><?php print $squad->price; ?></div>
                                <?php endif; ?>
                                <?php if (empty($hide_edit)):?>
                                        <?php print l(t('Edit Squad'), 'squad/' . $nid, array('attributes' => array('class' => array('btn')))); ?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
             <span class="not-found--text"><?php print t("You don\'t have any Squads yet."); ?></span>
        <?php endif;?>
	</div>
    <!-- <div class="create-squad form-actions">
        <?php print l(t('Create new'), 'create-squad', array('attributes' => array('class' => 'btn'))); ?>
    </div> -->
</section>
<?php if(!empty($squads_users)): ?>
    <section class="profile-section profile-section--users bg--dark profile-section-connect">
	    <div class="squads-list">
        <?php foreach($squads_users as $squad_id => $squad_users): ?>
            <div class="squads-carousel">
                <?php if(!empty($squad_users['squad_name'])): ?>
                    <h2 class="block-title"><?php print $squad_users['squad_name']; ?>
                        <?php if( isset($squad_users['users_number']) ):?>
                            <span class="counter"> (<?php print $squad_users['users_number']; ?>)</span>
                        <?php endif;?>
                    </h2>
                    <?php if( isset($show_all_link) && $show_all_link ):?>
                        <?php if( isset($popup) && !empty($popup) && count($users) > 3 ):?>
                            <div class="view-all"><?php print ctools_modal_text_button(t('View All'), $popup, t('View All'));?></div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif; ?>
                <div class="users-wrapper users-wrapper2">
                    <?php if(!empty($squad_users['users'])):  $i = 1; ?>
                        <?php foreach($squad_users['users'] as $uid => $profile): ?>
                            <div class="user <?php print 'squad-users-' . $squad_id; ?>" data-number="<?php print $i; ?>">
                                <div class="photo-wrap photo-holder">
                                    <div class="user-photo view-profile">
                                        <?php print render($profile->image); ?>
                                        <a href="<?php print $profile->profile_link;?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
                                    </div>
                                    <!--User Raiting Block-->
                                    <?php if( isset($show_rate) && $show_rate ):?>
                                        <span class="rating"><?php print render($profile->rating); ?></span>
                                    <?php endif;?>
                                    <!--End of user Raiting block-->
                                </div>
                                <h3 class="user-name"><?php print render($profile->name) . ' ' . render($profile->lastname); ?></h3>
                                <?php if( isset($show_location) && $show_location ):?>
                                    <span class="location"><?php print render($profile->location); ?></span>
                                <?php endif; ?>
                                <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
                            </div>
                            <?php $i++; endforeach; ?>
                    <?php else: ?>
                        <span><?php print t('You don\'t have any Users yet.'); ?></span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
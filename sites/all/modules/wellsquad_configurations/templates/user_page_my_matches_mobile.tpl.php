<?php if(!empty($users)): ?>
  <?php
  // CTools modal window
  ctools_include('modal');
  ctools_include('ajax');
  ctools_modal_add_js();
  drupal_add_js(array(
    'CToolsModal' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 450,
        'height' => 350,
        'contentBottom' => 800),
    ),
  ), 'setting');
  ?>

<section class="profile-section profile-section--users bg--dark profile-section-connect profile-section-connect--mobile profile-section-connect--mobMyMatches">
  <?php if(!empty($squad_name)): ?>
    <h2 class="block-title"><?php print $squad_name; ?>
      <?php if( isset($user_number) ):?>
        <span class="counter"> (<?php print $user_number; ?>)</span>
      <?php endif;?>
    </h2>
  <?php endif; ?>
  <?php if( isset($show_all_link) && $show_all_link ):?>
    <?php if( isset($popup) && !empty($popup) && count($users) > 3 ):?>
      <div class="view-all"><?php print ctools_modal_text_button(t('View All'), $popup, t('View All'));?></div>
    <?php endif;?>
  <?php endif;?>
  <div class="users-wrapper--mob">
    <div class="blockMob-bg" style="background-image: url(<?php print $image_mob; ?>);"></div>
    <div class="my-matches-mob users-wrapper">
      <?php if(!empty($users)):  $i = 1; ?>
        <?php foreach($users as $uid => $profile): ?>
          <div class="user" data-number="<?php print $i; ?>">
            <div class="photo-wrap photo-holder">
              <div class="user-photo view-profile">
                <?php print render($profile->image); ?>
                <a href="<?php print render($profile->profile_link); ?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
              </div>
              <!--User Raiting Block-->
              <?php if( isset($show_rate) && $show_rate ):?>
                <span class="rating"><?php print render($profile->rating); ?></span>
              <?php endif;?>
              <!--End of user Raiting block-->
            </div>
            <h3 class="user-name"><span><?php print render($profile->name); ?></span><span><?php print render($profile->lastname); ?></span></h3>
            <?php if(!empty($show_prof) && !empty($profile->profession)):?>
              <span class="profession" ><?php print t('Profession: ');?></span><?php print render($profile->profession); ?>
            <?php endif; ?>
            <?php if( isset($show_location) && $show_location ):?>
              <span class="location"><?php print render($profile->location); ?></span>
            <?php endif; ?>
            <?php if(!empty($profile->uid)): ?>
              <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$profile->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
            <?php endif; ?>
          </div>
          <?php $i++; endforeach; ?>
      <?php else: ?>
        <div class="user user--notfound">
          <?php if(!empty($not_found_msg)): ?>
            <p><img src="sites/default/files/add_user.png"  class="empty_photo" ></p>
            <div class="user-notfound-msg">
              <p><?php print  $not_found_msg; ?></p>
            </div>
          <?php else: ?>
            <span><?php print t('Users not found'); ?></span>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="mobBlock-readMore"><?php print $data; ?></div>
</section>
<?php endif; ?>
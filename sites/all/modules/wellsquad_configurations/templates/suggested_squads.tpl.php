<section class="profile-section border-bottom">
    <h2 class="block-title"><?php print t('Suggested squads'); ?></h2>
    <?php if(!empty($products)):?>
        <div class="squads-wrapper can-buy">
            <?php if(!empty($products)):?>
                <?php foreach($products as $nid => $squad): ?>
                    <div id="squad-<?php print $nid; ?>" class="squod-item">
                        <div class="squod-block">
                            <?php if(!empty($squad->image)): ?>
                                <div class="squod-preview">
                                    <div class="squod-image"><?php print render($squad->image); ?></div>
                                    <?php print render($squad->squad_link); ?>
                                </div>
                            <?php endif; ?>
                            <h3 class="squod-title"><?php print render($squad->title); ?></h3>
                            <ul class="description-list">

                                <?php if(!empty($squad->squad_leader_name)): ?>
                                    <li>
                                        <div class="squod-length">
                                            <h5><?php print t('Squad Leader: ');?></h5>
                                            <?php print render($squad->squad_leader_name); ?>
                                        </div>
                                    </li>
                                <?php endif; ?>

                                <?php if(!empty($squad->length)): ?>
                                    <li>
                                        <div class="squod-length">
                                            <h5><?php print t('Length of time: ');?></h5>
                                            <?php print $squad->length; ?>
                                        </div>
                                    </li>
                                <?php endif; ?>

                                <?php if(!empty($squad->openings)): ?>
                                    <li>
                                        <h5><?php print t('Openings: ');?></h5>
                                        <div class="squod-openings"><?php print $squad->openings; ?></div>
                                    </li>
                                <?php endif; ?>

                                <div class="squad-bottom">
                                    <?php if(!empty($squad->price)): ?>
                                        <h5><?php print t('Price per month: ');?></h5>
                                        <div class="squod-price"><?php print $squad->price; ?></div>
                                    <?php endif; ?>
                                </div>

                            </ul>
                            <div class="buy">
                                <?php print render($squad->buy_link); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
            <?php endif;?>
        </div>
    <?php else: ?>
        <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
    <?php endif; ?>
</section>
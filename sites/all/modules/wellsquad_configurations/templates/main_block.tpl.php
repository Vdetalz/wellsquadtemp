<section class="profile-section profile-section-connect profile-section-connect--main" style="background-image: url(<?php print $image_desk_main; ?>);">
  <div class="main-block">
    <?php global $base_path; ?>
    <?php if (isset($show_read_more) && $show_read_more): ?>
      <?php if (isset($data_popup) && !empty($data_popup)): ?>
        <button class="btn-read-more js-showMore"><?php print t('Read More'); ?></button>
        <div class="boxMore js-moreBlock">
          <p><?php print $data; ?></p>
          <button class="boxMore-close js-moreClose"></button>
        </div>
      <?php endif; ?>
    <?php endif; ?>
        <h1 class="block-title"><?php print $block_name; ?></h1>
  </div>
</section>
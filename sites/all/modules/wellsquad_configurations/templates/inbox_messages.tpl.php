<?php if(!empty($inbox_messages) || !empty($sent_messages)): ?>
    <div class="message-area">
        <ul class="tabset">
            <li><a href="#tab1" class="active"><?php print t('Inbox'); ?></a></li>
            <li><a href="#tab2"><?php print t('Sent'); ?></a></li>
        </ul>

        <?php if(!empty($inbox_messages)): ?>
            <div class="message-box tab-content">
                <!-- Inbox messages -->
                <div class="inbox-messages tabs_default" id="tab1">
                    <ul class="messages">
                        <?php foreach($inbox_messages as $k => $m): ?>
                            <?php $class = "inbox-msg";
                            $li_class = FALSE;
                            if($m['message']->is_new == PRIVATEMSG_UNREAD) {
                                $li_class = TRUE;
                            } ?>

                            <li <?php if($li_class) print "class='unread'"; ?> data-message-id="<?php print $m['message']->mid; ?>" data-user-id="<?php print $m['message']->author->uid; ?>">
                                    <div class="profile-photo">
                                        <?php print drupal_render($m['profile_photo']); ?>
                                    </div>
                                    <?php $name = isset($m['name']) ? $m['name'] : $m['message']->author->name; ?>
                                    <strong class="msg-author"><?php print $name; ?></strong>
                                    <time datetime="<?php print date('Y-m-d', $m['message']->timestamp); ?>"><?php print date('m.d.Y', $m['message']->timestamp); ?></time>
                                    <div class="inbox">
                                        <!-- <div class="author"></div> -->
                                        <div class="short-message" data-user-id="<?php print $m['message']->author->uid; ?>">
                                            <?php print $m['message']->subject; ?>
                                        </div>
                                        <div class="reply-form">
                                            <div class="full-message"><?php print $m['message']->body; ?></div>
                                        </div>
                                    </div>
                                <!-- </a> -->
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php $reply_form['message_user_id']['#value'] = $inbox_messages[0]['message']->author->uid;
                    print drupal_render($reply_form); ?>
                </div>
            <?php endif; ?>

            <!-- Sent messages -->
            <div class="inbox-messages sent-messages" id="tab2">
                <ul class="messages tabset">
                    <?php foreach($sent_messages as $k => $m): ?>
                        <li data-message-id="<?php print $m['message']->mid; ?>">
                                <div class="profile-photo">
                                    <?php print drupal_render($m['profile_photo']); ?>
                                </div>
                                <strong class="msg-author"><?php print $m['name']; ?></strong>
                                <time datetime="<?php print date('Y-m-d', $m['message']->timestamp); ?>"><?php print date('m.d.Y', $m['message']->timestamp); ?></time>
                                <div class="inbox">
                                    <!-- <div class="author"></div> -->
                                    <p class="short-message" data-user-id="<?php print $m['message']->author->uid; ?>">
                                        <?php print $m['message']->subject; ?>
                                    </p>
                                    <div class="reply-form">
                                        <div class="full-message"><?php print $m['message']->body; ?></div>
                                    </div>
                                </div>
                            <!-- </a> -->
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="newmsg-buttons">
	<div class="btn-wrap">
		<?php // New message popup
		print l(t('COMPOSE A NEW MESSAGE'), 'modal/nojs/new_message', array('attributes' => array('class' => 'ctools-use-modal ctools-modal-modal-popup-medium')));
		?>
	</div>
    <?php if(!empty($classes_list)): ?>
        <div class="btn-wrap">
            <?php // New group message popup
            print l(t('COMPOSE A GROUP MESSAGE'), 'modal/nojs/new_group_message', array('attributes' => array('class' => 'ctools-use-modal ctools-modal-modal-popup-medium')));
            ?>
        </div>
    <?php endif; ?>
</div>
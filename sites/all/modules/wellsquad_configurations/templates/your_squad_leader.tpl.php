<div class="profile-rate">
    <h3><?php echo $title;?></h3>
    <p><span class="rate-heading"><?php print t('Your Squad Leader'); ?></span></p>
    <div class="profile-photo">
        <?php print render($profile->photo); ?>
    </div>
    <div class="rate">
        <?php  print render($profile->rate); ?>
    </div>
</div>
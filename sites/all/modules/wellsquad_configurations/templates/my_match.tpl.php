<div class="block--2 match border-bottom border-right bg--dark">

    <h2>
        <?php if($show_my):?><span class="font--normal"><?php echo t('My');?> </span><?php endif;?><?php echo t('Match');?>
        <span class="font--normal counter">
        <?php if(isset($data['matches']) && count($data['matches'])):?>
            (<?php echo count($data['matches']);?>)
        <?php endif;?>
        </span>
    </h2>

    <div class="subcategory-list">
    <?php if(isset($data['matches']) && count($data['matches'])):?>
        <?php foreach($data['matches'] as $value):?>
            <?php if($value->exercises != false):?>
                <span class="subcategory"><?php echo $value->exercises;?></span>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif;?>
    </div>

    <div class="user-holder">
        <div class="user-item">
            <div class="squod-image">
                <?php print drupal_render($data['user']['profile_photo']);?>
            </div>
            <h3 class="user-name">
                <?php print drupal_render($data['user']['first_name']); ?>
                <?php print drupal_render($data['user']['last_name']);?>
            </h3>
            <div class="male">
                <?php print strip_tags(drupal_render($data['user']['male']));?>
            </div>
            <div class="code">
                <?php print drupal_render($data['user']['location']); ?>
            </div>
        </div>
        <?php if(!isset($data['matches']) || !count($data['matches'])):?>
         <div class="user-item">
                <div class="squod-image profile-link2">
                            <?php if($show_my):?>
                <span class="image-label"><?php echo t('add');?><br /><?php echo t('your');?><br /><?php echo t('match');?></span>
                <?php else:?>
                <span class="image-label"><?php echo t('no');?><br /><?php echo t('matches');?></span>
                <?php endif;?>
                </div>
             <?php if($show_my):?>
                <div class="btn-wrap">
                    <?php print l(t('Create Match'), variable_get('ws_connections_link')); ?>
                </div>
             <?php endif;?>
            </div>
        <?php else:?>
            <div class="users-carousel">
            <?php foreach($data['matches'] as $uid => $value):?>
                <div class="user-item">
                    <div class="squod-image profile-link2">
                        <?php print drupal_render($value->image);?>
						<?php print l( t('View profile to connect'), '/profile/' . $uid, array(
                            'attributes' => array('class' => array('profile-link'))
                        ) );?>
                    </div>
                    <h3 class="user-name">
                        <?php print render($value->name); ?>
                        <?php print render($value->lastname);?>
                    </h3>
                    <div class="male">
                        <?php print strip_tags(drupal_render($value->male));?>
                    </div>
                    <div class="code">
                        <?php print render($value->location); ?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <?php endif;?>
    </div>
</div>
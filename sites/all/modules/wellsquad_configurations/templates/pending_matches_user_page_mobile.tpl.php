<section class="profile-section profile-section--users bg--dark profile-section-connect profile-section-connect--mobile profile-section-connect--mobPendMatches">
  <?php if(!empty($block_title)): ?>
    <h2 class="block-title">
      <?php print $block_title; ?>
    </h2>
  <?php endif; ?>
  <?php if( isset($show_all_link) && $show_all_link ):?>
    <?php if( isset($popup) && !empty($popup) && count($users) > 3 ):?>
      <div class="view-all"><?php print ctools_modal_text_button(t('View All'), $popup, t('View All'));?></div>
    <?php endif;?>
  <?php endif;?>
  <div class="users-wrapper--mob">
    <div class="blockMob-bg" style="background-image: url(<?php print $image_mob; ?>);"></div>
    <div class="users-wrapper">
      <?php if(!empty($pending_matches_users)):  $i = 1; ?>
        <?php foreach($pending_matches_users as $uid => $profile): ?>
          <div class="user" data-number="<?php print $i; ?>">
            <div class="photo-wrap photo-holder">
              <div class="user-photo view-profile">
                <?php print render($profile->image); ?>
                <a href="<?php print $profile->profile_link;?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
              </div>
            </div>
            <h3 class="user-name"><span><?php print render($profile->name) ?></span><span><?php print render($profile->lastname); ?></span></h3>
            <?php if(!empty($profile->uid)): ?>
              <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$profile->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
            <?php endif;  ?>
            <?php if (isset($data) && !empty($data)): ?>
              <div class="block-description"><?php print $data; ?></div>
            <?php endif; ?>
            <div class="btn-wrap">
              <a href="user/<?php print $current_user_id; ?>/relationships/requested/<?php print $profile->rid; ?>/approve?destination=user/<?php print $current_user_id; ?>/relationships/received">
                <?php print t('Approve'); ?>
              </a>
            </div>
          </div>
          <?php $i++; endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</section>
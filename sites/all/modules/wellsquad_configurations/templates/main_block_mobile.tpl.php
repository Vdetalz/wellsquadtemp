<section class="profile-section profile-section-connect profile-section-mobMain">
  <div class="main-block mobConnectMain">
    <div class="mobConnectMain-header" style="background-image: url(<?php print $image_mob_main; ?>)">
      <h1 class="block-title"><?php print $block_name; ?></h1>
    </div>
    <?php if (isset($data) && !empty($data)): ?>
      <div class="mobConnectMain-body"><?php print $data; ?></div>
    <?php endif; ?>
  </div>
</section>
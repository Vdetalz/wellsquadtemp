<?php
    // CTools modal window
    ctools_include('modal');
    ctools_include('ajax');
    ctools_modal_add_js();
    drupal_add_js(array(
            'CToolsModal' => array(
                'modalSize' => array(
                    'type' => 'fixed',
                    'width' => 450,
                    'height' => 350,
                    'contentBottom' => 800),
            ),
        ), 'setting');
?>

<div class="brand">

	<?php if($data['nike']['isset']):?>
        <div class="brand-item">
		<div class="brand-preview">
			<img src="<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-1.jpg" alt="Nike">
		</div>
		<div class="brand-holder">
			<h3><?php print t('Nike+');?></h3>
			<?php print l(t('Connect'), _wca_nike_get_auth_link(), array('attributes' => array('class' => 'ctools-use-modal')));?>
		</div>
    </div>
	<?php endif;?>

	<?php if($data['fitbit']['isset']):?>
        <div class="brand-item">
		<div class="brand-preview">
			<img src="<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-2.jpg" alt="Fitbit">
		</div>
		<div class="brand-holder">
			<h3><?php print t('Fitbit');?></h3>
			<?php print l( t('Connect'), _wca_get_fitbit_auth_link() );?>
		</div>
    </div>
	<?php endif;?>

	<?php if($data['jawbone']['isset']):?>
        <div class="brand-item">
		<div class="brand-preview">
			<img src="<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-3.jpg" alt="JawBone">
		</div>
		<div class="brand-holder">
			<h3><?php print t('Jawbone');?></h3>
			<?php print l(t('Connect'), _wca_jawbone_get_auth_link());?>
		</div>
    </div>
	<?php endif;?>

	<?php if($data['foursquare']['isset']):?>
    <div class="brand-item">
		<div class="brand-preview">
			<img src="<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-4.jpg" alt="FourSquare">
		</div>
		<div class="brand-holder">
			<h3><?php print t('Foursquare');?></h3>
			<?php print l(t('Connect'), _wca_foursquare_get_auth_link());?>
		</div>
    </div>
	<?php endif;?>

  <?php if(!$data['foursquare']['isset'] && !$data['jawbone']['isset'] && !$data['nike']['isset'] && !$data['fitbit']['isset']): ?>
    <?php print t('You didn\'t connect to the any services');?>
  <?php endif;?>

</div>
<?php if(!empty($users)): ?>
  <?php
  // CTools modal window
  ctools_include('modal');
  ctools_include('ajax');
  ctools_modal_add_js();
  drupal_add_js(array(
    'CToolsModal' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 450,
        'height' => 350,
        'contentBottom' => 800),
    ),
  ), 'setting');
  ?>

<section class="profile-section profile-section--users profile-section-connect profile-section-connect--mobile profile-section-connect--mobSqLead">
  <?php if(!empty($squad_name)): ?>
    <h2 class="block-title"><?php print $squad_name; ?></h2>
  <?php endif; ?>
  <?php if( isset($show_all_link) && $show_all_link ):?>
    <?php if( isset($popup) && !empty($popup) && count($users) > 3 ):?>
      <div class="view-all"><?php print ctools_modal_text_button(t('View All'), $popup, t('View All'));?></div>
    <?php endif;?>
  <?php endif;?>
  <div class="users-wrapper--mob">
    <div class="blockMob-bg" style="background-image: url(<?php print $image_mob; ?>);"></div>
    <div class="users-wrapper">
      <?php if(!empty($users)):  $i = 1; ?>
        <?php foreach($users as $uid => $profile): ?>
          <div class="user" data-number="<?php print $i; ?>">
            <div class="photo-wrap photo-holder">
              <div class="user-photo view-profile">
                <?php print render($profile->image); ?>
                <a href="<?php print render($profile->profile_link); ?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
              </div>
              <!--User Raiting Block-->
              <?php if (isset($show_rate) && $show_rate): ?>
                <?php $rating = strval($profile->rating);
                $rating_array = explode('.', $rating);
                if (count($rating_array) > 1) : ?>
                  <span class="rating"><span><?php echo $rating_array[0]; ?></span><span><?php echo '.' . $rating_array[1]; ?></span></span>
                <?php else : ?>
                  <span class="rating"><span><?php echo $rating_array[0]; ?></span></span>
                <?php endif; ?>
              <?php endif; ?>
              <!--End of user Raiting block-->
            </div>
            <h3 class="user-name"><span><?php print render($profile->name); ?></span><span><?php print render($profile->lastname); ?></span></h3>
            <?php if(!empty($profile->uid)): ?>
              <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$profile->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
            <?php endif; ?>
          </div>
          <?php $i++; endforeach; ?>
      <?php else: ?>
        <div class="user user--notfound">
          <?php if(!empty($not_found_msg)): ?>
            <p><img src="sites/default/files/add_user.png"  class="empty_photo" ></p>
            <div class="user-notfound-msg">
              <p><?php print  $not_found_msg; ?></p>
            </div>
          <?php else: ?>
            <span><?php print t('Users not found'); ?></span>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="mobBlock-readMore"><?php print $data; ?></div>
</section>
<?php endif; ?>
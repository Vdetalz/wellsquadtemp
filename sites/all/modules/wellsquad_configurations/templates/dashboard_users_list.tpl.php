<div id="squad_ranking" class="ranking">
<?php if($data): ?>
	<div class="squad-ranking">
	<?php if($title): ?><h3 class="block-title"><?php print $title; ?></h3><?php endif; ?>
		<ul class="ranking-list">
			<li><span class="weekly"><?php print t('Weekly Avg'); ?></span><span><?php print t('Monthly Total'); ?></span></li>
		<?php $i = 1; ?>
		<?php foreach($data as $key => $user): extract($user); ?>
			<li class="ranking-item">
					<span class="user-count"><?php print $i; ?></span>
					<?php if(!empty($img)): ?><div class="user-photo"><img src="<?php print $img; ?>" /></div><?php endif; ?>
					<?php if(!empty($name)): ?><h5><?php print $name; ?></h5><?php endif; ?>
					<?php if(!empty($weekly_avg)): ?><span class="weekly-avg"><?php print $weekly_avg; ?></span><?php endif; ?>
					<?php if(!empty($monthly_avg)): ?><span class="monthly_avg"><?php print $monthly_avg; ?></span><?php endif; ?>
			</li>
			<?php $i++; ?>
		<?php endforeach;?>
		</ul>
	</div>
<?php else:?>
    <div class="squad-ranking">
        <?php if($title): ?><h3 class="block-title"><?php print $title; ?></h3><?php endif; ?>
    </div>
<?php print t('No Ranking data');?>
<?php endif;?>
</div>
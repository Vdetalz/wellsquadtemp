<section class="profile-section border-bottom">
    <?php if(!empty($squads)):?>
	<div class="squads-wrapper">
    <?php if(!empty($squads)):?>
		<?php foreach($squads as $nid => $squad): ?>
			<div id="squad-<?php print $nid; ?>" class="squod-item">
				<div class="squod-block">
					<?php if(!empty($squad->image)): ?>
						<div class="squod-preview">
							<div class="squod-image"><?php print render($squad->image); ?></div>
							<?php if(!empty($squad->user_number)): ?>
								<div class="squod-number">
									<span class="user-number"><?php print $squad->user_number; ?></span>
									<span>users</span>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<h3 class="squod-title"><?php print $squad->title; ?></h3>
					<ul class="description-list">
						<?php if(!empty($squad->tagline)): ?>
						<li>
							<h5 class="squod-tagline"><?php print $squad->tagline; ?></h5>
						</li>
						<?php endif; ?>

                        <?php if(!empty($squad->description)): ?>
                            <li>
                                <h5><?php print t('Description: ');?></h5>
                                <div class="squod-description"><?php print $squad->description; ?></div>
                            </li>
                        <?php endif; ?>

						<?php if(!empty($squad->length)): ?>
						<li>
							<div class="squod-length">
								<h5><?php print t('Length of time: ');?></h5>
								<?php print $squad->length; ?>
							</div>
						</li>
						<?php endif; ?>

                        <?php if(!empty($squad->field_equipment)): ?>
                            <li>
                                <h5><?php print t('Equipment: ');?></h5>
                                <div class="squod-equipment"><?php print render($squad->field_equipment); ?></div>
                            </li>
                        <?php endif; ?>

                        <?php if(!empty($squad->field_squad_goals)): ?>
                            <li>
                                <h5><?php print t('Goals: ');?></h5>
                                <div class="squod-goals"><?php print $squad->field_squad_goals; ?></div>
                            </li>
                        <?php endif; ?>

                        <?php if(!empty($squad->program_start)): ?>
                            <li>
                                <h5><?php print t('Program Start: ');?></h5>
                                <div class="squod-schedule"><?php print $squad->program_start; ?></div>
                            </li>
                        <?php endif; ?>

                        <?php if(!empty($squad->program_end)): ?>
                            <li>
                                <h5><?php print t('Program End: ');?></h5>
                                <div class="squod-schedule"><?php print $squad->program_end; ?></div>
                            </li>
                        <?php endif; ?>

						<?php if(!empty($squad->schedule)): ?>
						<li>
							<h5><?php print t('Schedule: ');?></h5>
							<div class="squod-schedule"><?php print $squad->schedule; ?></div>
						</li>
						<?php endif; ?>

						<?php if(!empty($squad->squad_offers)): ?>
						<li>
							<h5><?php print t('Squad Offers: ');?></h5>
							<div class="squod-squad-offers"><?php print $squad->squad_offers; ?></div>
						</li>
						<?php endif; ?>

						<?php if(!empty($squad->location)): ?>
						<li>
							<h5><?php print t('Location: ');?></h5>
							<div class="squod-location"><?php print $squad->location; ?></div>
						</li>
						<?php endif; ?>

						<?php if(!empty($squad->openings)): ?>
						<li>
							<h5><?php print t('Openings: ');?></h5>
							<div class="squod-openings"><?php print $squad->openings; ?></div>
						</li>
                        <div class="squad-bottom">
                            <?php if(!empty($squad->price)): ?>
                                    <h5><?php print t('Price per month: ');?></h5>
                                    <div class="squod-price"><?php print $squad->price; ?></div>
                            <?php endif; ?>
                            <?php if (empty($hide_edit)):?>
                                    <?php print l(t('Edit Squad'), 'squad/' . $nid, array('attributes' => array('class' => array('btn')))); ?>
                            <?php endif;?>
                        </div>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		<?php endforeach; ?>
    <?php else: ?>
        <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
    <?php endif;?>
	</div>
    <?php else: ?>
        <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
    <?php endif; ?>
    <?php if(empty($hide_edit)):?>
        <div class="create-squad form-actions">
            <?php print l(t('Create new'), 'create-squad', array('attributes' => array('class' => 'btn'))); ?>
        </div>
    <?php endif;?>
</section>
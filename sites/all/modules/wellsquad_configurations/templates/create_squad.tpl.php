<form accept-charset="UTF-8" id="product-node-form" method="post" action="/create-squad" enctype="multipart/form-data" class="node-form node-product-form">
<section class="profile-section border-bottom">
    <h4><?php print render($product_form['#groups']['group_basic_info']->label); ?></h4>
    <div class="profile-box--centered2 profile-box--centered4">
        <div class="profile-photo-wrap">
            <span class="upload-label">
                <?php print t('Squad'); ?><br /><?php print t('Photo'); ?>
            </span>
            <?php print render($product_form['uc_product_image']); ?>
        </div>
        <div class="col-container col-container3">
            <div class="form-item">
                <?php print render($product_form['title']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_type_of_exercise']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_squad_tagline']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_squad_description']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_squad_goals']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_program_start']); ?>
            </div>
            <div class="form-item">
                <?php print render($product_form['field_program_end']); ?>
            </div>
        </div>
    </div>
</section>
<section class="profile-section profile-section--noheading">
    <h4><?php print render($product_form['#groups']['group_features']->label); ?></h4>
    <div class="profile-box--centered2 profile-box--centered4">
        <div class="col-container">
            <div class="form-row">
                <div class="row-box">
                    <div class="row-item">
                        <?php print render($product_form['field_length_of_time']); ?>
                    </div>
                    <div class="row-item">
                        <?php print render($product_form['field_schedule_']); ?>
                    </div>
                </div>
            </div>
            <div class="form-row field-offers">
                <div class="row-box">
                    <?php print render($product_form['field_squad_offers_']); ?>
                </div>
            </div>
            <div class="form-row field-location">
                <div class="row-box">
                    <?php print render($product_form['field_location_']); ?>
                </div>
            </div>
            <div class="form-row field-equipment">
                <div class="row-box">
                    <?php print render($product_form['field_equipment_']); ?>
                </div>
            </div>
            <div class="form-row">
                <div class="row-box">
                    <div class="row-item">
                        <?php print render($product_form['base']['prices']['sell_price']);?>
                    </div>
                    <div class="row-item">
                        <?php print render($product_form['field_openings_']); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php  print render($product_form['actions']);?>
    </div>
</section>
<?php  print drupal_render_children($product_form); ?>
</form>
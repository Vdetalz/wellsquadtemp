<section class="profile-section profile-section--users profile-section-connect profile-section-connect--desktop profile-section-connect--style1 profile-section-connect--pendingMatches" style="background-image: url(<?php print $image_desk; ?>);">
  <?php if(!empty($block_title)): ?>
    <h2 class="block-title"><?php print $block_title; ?></h2>
  <?php endif; ?>
  <?php if (isset($show_read_more) && $show_read_more): ?>
    <?php if (isset($data_popup) && !empty($data_popup)): ?>
      <button class="btn-read-more js-showMore"><?php print t('Read More'); ?></button>
      <div class="boxMore js-moreBlock">
        <p><?php print $data; ?></p>
        <button class="boxMore-close js-moreClose"></button>
      </div>
    <?php endif; ?>
  <?php endif; ?>
  <div class="users-wrapper">
    <?php if(!empty($pending_matches_users)):  $i = 1; ?>
      <?php foreach($pending_matches_users as $uid => $profile): ?>
        <div class="user" data-number="<?php print $i; ?>">
          <div class="photo-wrap photo-holder">
            <div class="user-photo view-profile">
              <?php print render($profile->image); ?>
              <a href="<?php print $profile->profile_link;?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
            </div>
            <?php if(!empty($profile->uid)): ?>
              <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$profile->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
            <?php endif;  ?>
          </div>
          <h3 class="user-name"><span><?php print render($profile->name); ?></span><span><?php print render($profile->lastname); ?></span></h3>
          <div class="btn-wrap">
            <a href="user/<?php print $current_user_id; ?>/relationships/requested/<?php print $profile->rid; ?>/approve?destination=user/<?php print $current_user_id; ?>/relationships/received">
              <?php print t('Approve'); ?>
            </a>
          </div>
        </div>
        <?php $i++; endforeach; ?>
    <?php endif; ?>
  </div>
</section>
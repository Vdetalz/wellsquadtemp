<section class="profile-section profile-section--users bg--dark profile-section-connect">
    <?php if(!empty($block_title)): ?>
        <h2 class="block-title">
            <?php print $block_title; ?>
        </h2>
    <?php endif; ?>
    <div class="users-wrapper users-wrapper2">
        <?php if(!empty($pending_matches_users)):  $i = 1; ?>
            <?php foreach($pending_matches_users as $uid => $profile): ?>
                <div class="user" data-number="<?php print $i; ?>">
                    <div class="photo-wrap photo-holder">
                        <div class="user-photo view-profile">
                            <?php print render($profile->image); ?>
                            <a href="<?php print $profile->profile_link;?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
                        </div>
                    </div>
                    <h3 class="user-name"><?php print render($profile->name) . ' ' . render($profile->lastname); ?></h3>
                    <?php if(!empty($profile->uid)): ?>
                      <div class="send-email"><?php print l('Email link', "modal/nojs/message/{$profile->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
                    <?php endif;  ?>
                    <div class="btn-wrap">
                        <a href="user/<?php print $current_user_id; ?>/relationships/requested/<?php print $profile->rid; ?>/approve?destination=user/<?php print $current_user_id; ?>/relationships/received">
                            <?php print t('Approve'); ?>
                        </a>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
        <?php endif; ?>
    </div>
</section>
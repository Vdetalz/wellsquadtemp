<?php if(!empty($users)):
    // CTools modal window
    ctools_include('modal');
    ctools_include('ajax');
    ctools_modal_add_js();
    drupal_add_js(array(
            'CToolsModal' => array(
                'modalSize' => array(
                    'type' => 'fixed',
                    'width' => 450,
                    'height' => 350,
                    'contentBottom' => 800),
            ),
        ), 'setting');

    ?>

    <section class="profile-section profile-section--users bg--dark">
        <?php if(!empty($squad_name)): ?>
            <h2 class="block-title"><?php print $squad_name; ?>
                <?php if( isset($user_number) ):?>
                    <span class="counter"> (<?php print $user_number; ?>)</span>
                <?php endif;?>
            </h2>
            <?php if( isset($show_all_link) && $show_all_link ):?>

                <?php if( isset($popup) && !empty($popup) ):?>
                    <div class="view-all"><?php print ctools_modal_text_button(t('View All'), $popup, t('View All'));?></div>
                <?php endif;?>

            <?php endif;?>
        <?php endif; ?>
        <div class="users-wrapper users-wrapper2 carousel-double">
            <?php $i = 1; ?>
            <?php foreach($users as $uid => $user): ?>
                <?php if($i == 1 || $i%2 == 1): ?>
                    <div class="users-col">
                <?php endif; ?>
                    <div id="user-<?php print $uid; ?>" class="user">
                        <div class="user-photo view-profile">
                            <?php print render($user->image); ?>
                            <a href="<?php print render($user->profile_link); ?>" class="profile-link"><?php print t('View profile to connect'); ?></a>
                        </div>
                        <h3 class="user-name"><?php print render($user->name) . ' ' . render($user->lastname); ?></h3>

                        <?php if( isset($show_location) && $show_location ):?>
                            <span class="location"><?php print render($user->location); ?></span>
                        <?php endif;?>

			<div id="send-email" class="send-email"><?php print l('Email link', "modal/nojs/message/{$uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?></a></div>
                    </div>
                <?php if($i%2 == 0 || $i == count($users)): ?>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>
            <?php endforeach;  ?>
        </div>
    </section>
<?php endif; ?>

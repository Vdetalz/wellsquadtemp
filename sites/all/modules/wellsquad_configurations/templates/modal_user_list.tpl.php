<div class="all-squads">
    <h3><?php echo $title;?></h3>
    <ul class="all-squads-list">
        <?php foreach($users as $uid => $user):?>
            <li>
                <div class="user-photo">
                    <?php print render($user->image); ?>
                </div>
                <div class="squads-info">
                    <h5>
                        <?php print render($user->name); ?>
                        <?php print render($user->lastname); ?>
                        <?php if(!$hide_rating): ?>
                            <span class="counter">(<?php print $user->rating; ?>)</span>
                        <?php endif; ?>
                    </h5>
                    <span class="profile-anchor"><?php print l(t('View Profile'), $user->profile_link); ?></span>
                    <span class="message-anchor">
                        <?php print l('Send Message', "modal/nojs/message/{$user->uid}", array('attributes' => array('class' => 'ctools-use-modal'))); ?>
                    </span>

                </div>
            </li>
        <?php endforeach;?>
    </ul>
</div>
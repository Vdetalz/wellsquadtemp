<?php if(!empty($user_info)): ?>
    <section class="profile-section profile-section2 border-bottom bg--dark">
        <h2 class="heading--styled">
            <span><?php print t('My'); ?></span>
            <?php print t('Profile'); ?>
        </h2>
        <div class="profile-info">
            <div class="profile-cart">
            <?php if(!empty($user_info->gender)):?>
                <div class="photo-wrap">
                    <div class="profile-photo">
                        <?php print render($user_info->photo); ?>
                    </div>
                    <span class="rating">
                        <?php print render($user_info->rating); ?>
                    </span>
                </div>
            <?php endif; ?>
            <section class="param-holder">
                <h3><?php print render($user_info->name); ?></h3>
                <?php if(!empty($user_info->gender)):?>
                    <div class="gender param-row">
                        <span><?php print t('Gender: ');?></span><?php print render($user_info->gender); ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($user_info->profession)):?>
                  <div class="url param-row">
                    <span><?php print t('Profession: ');?></span><?php print render($user_info->profession); ?>
                  </div>
                <?php endif; ?>
                <?php if(!empty($user_info->location)):?>
                    <div class="location param-row">
                        <span><?php print t('Location: ');?></span><?php print render($user_info->location); ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($user_info->url)):?>
                    <div class="url param-row">
                        <span><?php print t('URL: ');?></span><?php print render($user_info->url); ?>
                    </div>
                <?php endif; ?>
            </section>
            </div>
            <article class="profile-article">
            <?php if(!empty($user_info->description)):?>
                <h3><?php print t('Choose from our selection of experts, we call Squad Leaders, to jump start your results!'); ?></h3>
                <div class="profile-desc">
                    <?php print render($user_info->description); ?>
                </div>
            <?php endif; ?>
            </article>
            <div class="more">
                <?php print render($user_info->edit_link); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<section class="profile-section border-bottom">
    <h2 class="squads-title"><?php print t('My'); ?> <strong><?php print t('Classes'); ?></strong></h2>
    <div class="squads-wrapper">
        <?php if(!empty($squads)):?>
            <?php foreach($squads as $nid => $squad): ?>
                <div id="squad-<?php print $nid; ?>" class="squod-item">
                    <div class="squod-block">
                        <?php if(!empty($squad->image)): ?>
                            <div class="squod-preview">
                                <div class="squod-image"><?php print render($squad->image); ?></div>
								<?php print render($squad->squad_link); ?>
                                <?php if(!empty($squad->user_number)): ?>
                                    <div class="squod-number">
                                        <span class="user-number"><?php print $squad->user_number; ?></span>
                                        <span><?php print t('users'); ?></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <h3 class="squod-title"><?php print $squad->title; ?></h3>
                        <ul class="description-list">
                            <?php if(!empty($squad->tagline)): ?>
                                <li>
                                    <h5 class="squod-tagline"><?php print $squad->tagline; ?></h5>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->description)): ?>
                                <li>
                                    <h5><?php print t('Description: ');?></h5>
                                    <div class="squod-description"><?php print $squad->description; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->length)): ?>
                                <li>
                                    <div class="squod-length">
                                        <h5><?php print t('Length of time: ');?></h5>
                                        <?php print $squad->length; ?>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->schedule)): ?>
                                <li>
                                    <h5><?php print t('Schedule: ');?></h5>
                                    <div class="squod-schedule"><?php print $squad->schedule; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->squad_offers)): ?>
                                <li>
                                    <h5><?php print t('Squad Offers: ');?></h5>
                                    <div class="squod-squad-offers"><?php print $squad->squad_offers; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->location)): ?>
                                <li>
                                    <h5><?php print t('Location: ');?></h5>
                                    <div class="squod-location"><?php print $squad->location; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->price)): ?>
                                <li>
                                    <h5><?php print t('Price per month: ');?></h5>
                                    <div class="squod-price"><?php print $squad->price; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->openings)): ?>
                                <li>
                                    <h5><?php print t('Openings: ');?></h5>
                                    <div class="squod-openings"><?php print $squad->openings; ?></div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <?php print t('Squads not found'); ?>
        <?php endif; ?>
    </div>
    <div class="create-squad form-actions">
        <?php print l(t('Create new'), 'create-squad', array('attributes' => array('class' => 'btn'))); ?>
    </div>
</section>
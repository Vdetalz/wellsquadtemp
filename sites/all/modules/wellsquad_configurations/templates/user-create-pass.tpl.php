<section class="profile-section border-bottom">
    <h4><?php print t('Profile password'); ?></h4>
    <div class="profile-box--centered">
        <div class="col-wrap">
            <div class="col-holder">
                <div class="profile-col">
                    <?php echo render($form); ?>
                </div>
            </div>
        </div>
    </div>
</section>
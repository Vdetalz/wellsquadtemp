<?php
    if(!empty($squads_users)){
    // CTools modal window
        ctools_include('modal');
        ctools_include('ajax');
        ctools_modal_add_js();
        drupal_add_js(array(
                'CToolsModal' => array(
                    'modalSize' => array(
                        'type' => 'fixed',
                        'width' => 450,
                        'height' => 350,
                        'contentBottom' => 800),
                ),
            ), 'setting');
    }
?>
<?php if(!empty($squad_leaders)): ?>
    <section class="profile-section profile-section2 border-bottom profile-section-new">
        <h2 class="heading--styled profile-heading">
            <?php print t('Squad Leader'); ?>
        </h2>
        <div class="leader-list">
        <?php foreach($squad_leaders as $squad_leader): ?>
            <div class="profile-info">
                <div class="profile-cart">
                    <div class="photo-wrap">
                        <div class="profile-photo">
                            <?php print render($squad_leader->photo); ?>
                        </div>
                    </div>
                    <section class="param-holder">
                        <div class="param-info">
                        <h3><?php print render($squad_leader->name); ?></h3>
                        <?php if(!empty($squad_leader->gender)):?>
                            <div class="gender param-row">
                                <span><?php print t('Gender: ');?></span><?php print render($squad_leader->gender); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($squad_leader->age)):?>
                            <div class="age param-row">
                                <span><?php print t('Age: ');?></span><?php print render($squad_leader->age); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($squad_leader->location)):?>
                            <div class="location param-row">
                                <span><?php print t('Location: ');?></span><?php print render($squad_leader->location); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($squad_leader->profession)): ?>
                            <div class="profession param-row">
                                <span><?php print t('Profession: ');?></span><?php print render($squad_leader->profession); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($squad_leader->experience)):?>
                            <div class="experience param-row">
                                <span><?php print t('Years of Experience: ');?></span><?php print render($squad_leader->experience); ?>
                            </div>
                        <?php endif; ?>
                        </div>
                        <div class="buttons-section buttons-section__rate">
                            <?php print ctools_modal_text_button(t('Rate Your Squad Leader'), $popup . '/profile/' . $squad_leader->uid, t('Rate Your Squad Leader'), 'rate-btn btn');?>
                            <?php print l('Send Message', "modal/nojs/message/" . $squad_leader->uid, array('attributes' => array('class' => 'ctools-use-modal btn'))); ?>
                            <?php print l(t('Send group message'), 'modal/nojs/new_group_message', array('attributes' => array('class' => 'btn ctools-use-modal ctools-modal-modal-popup-medium'))); ?>
                        </div>
                    </section>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
<section class="profile-section profile-section-squad bg--dark">
    <?php if(!empty($squads)): ?>
	<div class="squads-wrapper-profile_user">
    <?php if(!empty($squads)):?>
		<?php foreach($squads as $nid => $squad): ?>
			<div id="squad-<?php print $nid; ?>" class="squod-item">
				<div class="squod-block border-bottom">
					<?php if(!empty($squad->image)): ?>
						<div class="squod-preview border-right">
							<div class=""><?php print render($squad->image); ?></div>
                        </div>
                    <?php endif; ?>
                    <div class="squod-description">
                        <div class="squod-about border-bottom">
                            <h3 class="squod-title">
                                <?php print $squad->title; ?>
        						<?php if(!empty($squad->user_number)): ?>
        							<span>( <?php print $squad->user_number; ?><?php print t(' users'); ?> )</span>
        						<?php endif; ?>
                            </h3>
        					<ul class="description-list">
                                <?php if(!empty($squad->description)): ?>
                                    <li>
                                        <h5><?php print t('Description: ');?></h5>
                                        <div class="squod-description"><?php print $squad->description; ?></div>
                                    </li>
                                <?php endif; ?>

        						<?php if(!empty($squad->length)): ?>
        						<li>
        							<div class="squod-length">
        								<h5><?php print t('Length of time: ');?></h5>
        								<?php print $squad->length; ?>
        							</div>
        						</li>
        						<?php endif; ?>

                                <?php if(!empty($squad->field_equipment)): ?>
                                    <li>
                                        <h5><?php print t('Equipment: ');?></h5>
                                        <div class="squod-equipment"><?php print render($squad->field_equipment); ?></div>
                                    </li>
                                <?php endif; ?>

                                <?php if(!empty($squad->field_squad_goals)): ?>
                                    <li>
                                        <h5><?php print t('Goals: ');?></h5>
                                        <div class="squod-goals"><?php print $squad->field_squad_goals; ?></div>
                                    </li>
                                <?php endif; ?>

                                <?php if(!empty($squad->program_start)): ?>
                                    <li>
                                        <h5><?php print t('Program Start: ');?></h5>
                                        <div class="squod-schedule"><?php print $squad->program_start; ?></div>
                                    </li>
                                <?php endif; ?>

                                <?php if(!empty($squad->program_end)): ?>
                                    <li>
                                        <h5><?php print t('Program End: ');?></h5>
                                        <div class="squod-schedule"><?php print $squad->program_end; ?></div>
                                    </li>
                                <?php endif; ?>

        						<?php if(!empty($squad->schedule)): ?>
        						<li>
        							<h5><?php print t('Schedule: ');?></h5>
        							<div class="squod-schedule"><?php print $squad->schedule; ?></div>
        						</li>
        						<?php endif; ?>

        						<?php if(!empty($squad->squad_offers)): ?>
        						<li>
        							<h5><?php print t('Squad Offers: ');?></h5>
        							<div class="squod-squad-offers"><?php print $squad->squad_offers; ?></div>
        						</li>
        						<?php endif; ?>

        						<?php if(!empty($squad->location)): ?>
        						<li>
        							<h5><?php print t('Location: ');?></h5>
        							<div class="squod-location"><?php print $squad->location; ?></div>
        						</li>
        						<?php endif; ?>

        						<?php if(!empty($squad->openings)): ?>
        						<li>
        							<h5><?php print t('Openings: ');?></h5>
        							<div class="squod-openings"><?php print $squad->openings; ?></div>
        						</li>
        						<?php endif; ?>
        					</ul>
                        </div>
                    </div>
				</div>
			</div>
		<?php endforeach; ?>
    <?php else: ?>
        <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
    <?php endif;?>
	</div>
    <?php else: ?>
        <span class="not-found--text"><?php print t('You don\'t have any Squads yet.'); ?></span>
    <?php endif; ?>
</section>
<?php
/**
 * Implements hook_rules_event_info().
 */
function wellsquad_configurations_rules_event_info() {
    $events['squad_leader_profile_approve'] = array(
        'label' => t('Profile approve'),
        'group' => t('Wellsquad'),
        'variables' => array(
            'account' => array(
                'type' => 'user',
                'label' => t('User'),
            ),
        ),
    );

    return $events;
}
<?php

function users_export_data($form, &$form_state) {

    $form['description'] = array(
        '#type' => 'item',
        '#title' => t('On this page you can export users data.'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export'),
        '#submit' => array('export_users_data_form_submit'),
    );

    //Files list table
    $dir = drupal_realpath('public://profiles_xls');
    $files = file_scan_directory($dir, '/.*\.xls$/');


    if(!empty($files)){
        $form['table'] = array(
            '#tree' => TRUE,
            '#theme' => 'tabular_form', // table function
        );

        $form['table']['header'] = array(
            '#type' => 'value',
            '#value' => array(t('Date'), t('File name'), t('Action')),
        );

        foreach ($files as $file) {
            if(empty($file->filename) || empty($file->name)){
                continue;
            }

            $uri = 'public://';
            $path= file_create_url($uri);

            //Get Date from file name
            preg_match('/(\d{2})-(\d{2})-(\d{4})/', $file->filename, $date_value);

            $form['table']['data'][$file->name]['created'] = array(
                '#markup' => !empty($date_value[0])? $date_value[0]: '',
            );

            $form['table']['data'][$file->name]['file_name'] = array(
                '#markup' => l($file->filename, $path . 'profiles_xls/' . $file->filename),
            );

            $form['table']['data'][$file->name]['remove'] = array(
                '#type'      => 'submit',
                '#value'     => t('Remove'),
                '#name'      => $file->name,
                '#file_uri' => $file->filename,
                '#submit'    => array('users_export_data_remove_submit'),
            );
        }
    }

    return $form;
}


function users_export_data_remove_submit($form, &$form_state) {
    if(empty($form_state['triggering_element']['#file_uri'])){
        return;
    }

    $uri = 'public://profiles_xls/' . $form_state['triggering_element']['#file_uri'];
    $path= drupal_realpath($uri);

    //remove file
    if(file_exists($path)){
        unlink($path);
        drupal_set_message('File has been removed.');
    }

    return;

}

function get_profiles_list(){

    $all_users = entity_load('user');
    $req_user_rows = array(
        'uid'                        => '',
        'role'                       => '',
        'name'                       => '',
        'surname'                    => '',
        'email'                      => '',
        'male'                       => '',
        'age'                        => '',
        'location'                   => '',
        'body_type'                  => '',
        'zip_code'                   => '',
        'relationship_status'        => '',
        'partner_male'               => '',
        'workout_partner'            => '',
        'my_schedule'                => '',
        'prefer_workout_in'          => '',
        'consider_myself'            => '',
        'partner_to_be'              => '',
        'fitness_experience'         => '',
        'partners_experience'        => '',
        'preferred_workout_location' => '',
        'longterm_goal'              => '',
        'exercise_favourite_type'    => '',
        'nutrition_services'         => '',
        'motivation_level'           => '',
        'method_of_tracking'         => '',
    );

    $req_squad_leader_rows = array(
        'uid'                     => '',
        'name'                    => '',
        'surname'                 => '',
        'email'                   => '',
        'phone_number'            => '',
        'male'                    => '',
        'location'                => '',
        'age'                     => '',
        'tagline'                 => '',
        'url'                     => '',
        'zip_code'                => '',
        'kind_of_professional'    => '',
        'fitness_experience'      => '',
        'exercise_favourite_type' => '',
        'accomplishments'         => '',
        'classes_per_month'       => '',
        'users_per_class'         => '',
        'reference_1_name'        => '',
        'reference_1_email'       => '',
        'reference_2_name'        => '',
        'reference_2_email'       => '',
    );

    foreach($all_users as $uid => $user_data){
        if(in_array('user', $user_data->roles)){

          $profile2_info_1 = profile2_load_by_user($uid, 'user_info_1');
          if(!empty($profile2_info_1)) {
            $row['users'][$uid] = user_profile_data($uid, $req_user_rows, $profile2_info_1);
          }
          else{
            $row['user_without_profile'][$uid] = user_without_profile_data($uid);
          }
        }elseif(in_array('squad leader', $user_data->roles)){
            $row['squad_leaders'][$uid] = squad_leader_profile_data($uid, $req_squad_leader_rows);
        }
    }

    return $row;
}

function user_profile_data($uid, $rows = array(), $profile2_info_1){

        $rows['uid'] = $uid;
        $rows['role'] = t('user');

        //First Name
        $first_name = field_get_items('profile2', $profile2_info_1, 'field_first_name');
        if (!empty($first_name[0]['value'])) {
            $rows['name'] = $first_name[0]['value'];
        }

        //Last Name
        $last_name = field_get_items('profile2', $profile2_info_1, 'field_last_name');
        if (!empty($last_name[0]['value'])) {
            $rows['surname'] = $last_name[0]['value'];
        }

        $user_obj = user_load($uid);
        if (!empty($user_obj->mail)) {
          $rows['email'] = $user_obj->mail;
        }


        //User Male
        $user_male_term = field_get_items('profile2', $profile2_info_1, 'field_male');
        $user_male = taxonomy_term_load($user_male_term[0]['tid']);

        if (!empty($user_male)) {
            $rows['male'] = $user_male->name;
        }

        //User Location
        $location = field_get_items('profile2', $profile2_info_1, 'field_location');
        if (!empty($location[0]['value'])) {
            $rows['location'] = $location[0]['value'];
        }

        //User Age
        $user_age = field_get_items('profile2', $profile2_info_1, 'field_age_range');
        if(!empty($user_age)){
            $rows['age'] = $user_age[0]['value'];
        }

        //Body Type
        $body_type_term = field_get_items('profile2', $profile2_info_1, 'field_body_type');
        $body_type = taxonomy_term_load($body_type_term[0]['tid']);

        if (!empty($body_type)) {
            $rows['body_type'] = $body_type->name;
        }

        //Zip Code
        $zip_code = field_get_items('profile2', $profile2_info_1, 'field_zip_code');
        if (!empty($zip_code[0]['value'])) {
            $rows['zip_code'] = $zip_code[0]['value'];
        }

        //User Relationship Status
        $relationship_status = field_get_items('profile2', $profile2_info_1, 'field_relationship_status');
        $user_relationship_status = taxonomy_term_load($relationship_status[0]['tid']);

        if (!empty($user_relationship_status->name)) {
            $rows['relationship_status'] = $user_relationship_status->name;
        }

        //Partner Male
        $partner_male_term = field_get_items('profile2', $profile2_info_1, 'field_partner_male');
        $partner_male = taxonomy_term_load($partner_male_term[0]['tid']);

        if (!empty($partner_male->name)) {
            $rows['partner_male'] = $partner_male->name;
        }

        //Prefer Workout Partner
        $workout_partner_term = field_get_items('profile2', $profile2_info_1, 'field_prefer_workout_partner');
        $workout_partner = taxonomy_term_load($workout_partner_term[0]['tid']);

        if (!empty($workout_partner->name)) {
            $rows['workout_partner'] = $workout_partner->name;
        }


        //Prefer Workout Partner
        $my_schedule_term = field_get_items('profile2', $profile2_info_1, 'field_my_schedule');
        $my_schedule = taxonomy_term_load($my_schedule_term[0]['tid']);

        if (!empty($my_schedule->name)) {
            $rows['my_schedule'] = $my_schedule->name;
        }

        //Prefer Workout Partner
        $prefer_workout_in_term = field_get_items('profile2', $profile2_info_1, 'field_prefer_workout_in');
        $prefer_workout_in = taxonomy_term_load($prefer_workout_in_term[0]['tid']);

        if (!empty($prefer_workout_in->name)) {
            $rows['prefer_workout_in'] = $prefer_workout_in->name;
        }

        //Consider Myself
        $consider_myself_term = field_get_items('profile2', $profile2_info_1, 'field_consider_myself');
        $consider_myself = taxonomy_term_load($consider_myself_term[0]['tid']);

        if (!empty($consider_myself->name)) {
            $rows['consider_myself'] = $consider_myself->name;
        }

        //Partner To Be
        $partner_to_be_term = field_get_items('profile2', $profile2_info_1, 'field_partner_to_be');
        $partner_to_be = taxonomy_term_load($partner_to_be_term[0]['tid']);

        if (!empty($partner_to_be->name)) {
            $rows['partner_to_be'] = $partner_to_be->name;
        }

        //Fitness Experience
        $fitness_experience_term = field_get_items('profile2', $profile2_info_1, 'field_fitness_experience');
        $fitness_experience = taxonomy_term_load($fitness_experience_term[0]['tid']);

        if (!empty($fitness_experience->name)) {
            $rows['fitness_experience'] = $fitness_experience->name;
        }

        //Partners Experience
        $partners_experience_term = field_get_items('profile2', $profile2_info_1, 'field_partners_experience');
        $partners_experience = taxonomy_term_load($partners_experience_term[0]['tid']);

        if (!empty($partners_experience->name)) {
            $rows['partners_experience'] = $partners_experience->name;
        }

        //Preferred Workout Location
        $preferred_workout_location_term = field_get_items('profile2', $profile2_info_1, 'field_preferred_workout_location');
        $preferred_workout_location = taxonomy_term_load($preferred_workout_location_term[0]['tid']);

        if (!empty($preferred_workout_location->name)) {
            $rows['preferred_workout_location'] = $preferred_workout_location->name;
        }

        //Longterm Goal
        $field_longterm_goal_term = field_get_items('profile2', $profile2_info_1, 'field_longterm_goal');
        $field_longterm_goal = taxonomy_term_load($field_longterm_goal_term[0]['tid']);

        if (!empty($field_longterm_goal->name)) {
            $rows['longterm_goal'] = $field_longterm_goal->name;
        }

        //Exercise Favourite Type
        $exercise_favourite_type = array();
        $field_exercise_favourite_type_term = field_get_items('profile2', $profile2_info_1, 'field_exercise_favourite_type');
        if(is_array($field_exercise_favourite_type_term)){
            foreach($field_exercise_favourite_type_term as $key => $term){
                $exercise_favourite_type_name = taxonomy_term_load($term['tid']);
                if(empty($exercise_favourite_type_name->name)){
                    continue;
                }
                $exercise_favourite_type[] = $exercise_favourite_type_name->name;
            }
        }

        if (!empty($exercise_favourite_type)) {
            $rows['exercise_favourite_type'] = implode(', ', $exercise_favourite_type);
        }

        //Nutrition Services
        $nutrition_services = field_get_items('profile2', $profile2_info_1, 'field_nutrition_services');
        if (!empty($nutrition_services[0]['value'])) {
            $rows['nutrition_services'] = $nutrition_services[0]['value'];
        }

    $profile2_info_2 = profile2_load_by_user($uid, 'user_info_2');

    if(!empty($profile2_info_2)){
        //Exercise Favourite Type
        $motivation_level_term = field_get_items('profile2', $profile2_info_2, 'field_motivation_level');
        $motivation_level = taxonomy_term_load($motivation_level_term[0]['tid']);

        if (!empty($motivation_level)) {
            $rows['motivation_level'] = $motivation_level->name;
        }

        //Exercise Favourite Type
        $method_of_tracking_term = field_get_items('profile2', $profile2_info_2, 'field_method_of_tracking');
        $method_of_tracking = taxonomy_term_load($method_of_tracking_term[0]['tid']);

        if (!empty($method_of_tracking)) {
            $rows['method_of_tracking'] = $method_of_tracking->name;
        }
    }

    return $rows;
}


function squad_leader_profile_data($uid, $rows = array()){
    $profile2_info_1 = profile2_load_by_user($uid, 'user_info_1_squad_leader');

    if(!empty($profile2_info_1)){
        $rows['uid'] = $uid;
        $req_rows['role'] = t('Squad Leader');
        //Squad Leader First Name
        $first_name = field_get_items('profile2', $profile2_info_1, 'field_first_name');
        if (!empty($first_name[0]['value'])) {
            $rows['name'] = $first_name[0]['value'];
        }

        //Squad Leader Last Name
        $last_name = field_get_items('profile2', $profile2_info_1, 'field_last_name');
        if (!empty($last_name[0]['value'])) {
            $rows['surname'] = $last_name[0]['value'];
        }

        $user_obj = user_load($uid);
        if (!empty($user_obj->mail)) {
          $rows['email'] = $user_obj->mail;
        }

        //Squad Leader Phone Number
        $phone_number = field_get_items('profile2', $profile2_info_1, 'field_phone_number');
        if (!empty($phone_number[0]['value'])) {
            $rows['phone_number'] = $phone_number[0]['value'];
        }

        //Squad Leader Male
        $user_male_term = field_get_items('profile2', $profile2_info_1, 'field_male');
        $user_male = taxonomy_term_load($user_male_term[0]['tid']);

        if (!empty($user_male)) {
            $rows['male'] = $user_male->name;
        }

        //Squad Leader Location
        $location = field_get_items('profile2', $profile2_info_1, 'field_location');
        if (!empty($location[0]['value'])) {
            $rows['location'] = $location[0]['value'];
        }

        //Squad Leader Age
        $user_age = field_get_items('profile2', $profile2_info_1, 'field_age_range');
        if(!empty($user_age)){
            $rows['age'] = $user_age[0]['value'];
        }


        // Squad Leader Tagline
        $tagline = field_get_items('profile2', $profile2_info_1, 'field_tagline');
        if(!empty($tagline[0]['value'])){
            $rows['tagline'] = $tagline[0]['value'];
        }

        // Squad Leader Tagline
        $url = field_get_items('profile2', $profile2_info_1, 'field_url_http_');
        if(!empty($url[0]['value'])){
            $rows['url'] = $url[0]['value'];
        }

        //Zip Code
        $zip_code = field_get_items('profile2', $profile2_info_1, 'field_zip_code');
        if (!empty($zip_code[0]['value'])) {
            $rows['zip_code'] = $zip_code[0]['value'];
        }

        //Kind of Professional
        $kind_of_professional_term = field_get_items('profile2', $profile2_info_1, 'field_kind_of_professional');
        $kind_of_professional = taxonomy_term_load($kind_of_professional_term[0]['tid']);

        if (!empty($kind_of_professional->name)) {
            $rows['kind_of_professional'] = $kind_of_professional->name;
        }

        //Exercise Favourite Type
        $exercise_favourite_type = array();
        $field_exercise_favourite_type_term = field_get_items('profile2', $profile2_info_1, 'field_exercise_favourite_type');
        if(is_array($field_exercise_favourite_type_term)) {
            foreach ($field_exercise_favourite_type_term as $key => $term) {
                $exercise_favourite_type_name = taxonomy_term_load($term['tid']);
                if (empty($exercise_favourite_type_name->name)) {
                    continue;
                }

                $exercise_favourite_type[] = $exercise_favourite_type_name->name;
            }
        }

        if (!empty($exercise_favourite_type)) {
            $rows['exercise_favourite_type'] = implode(', ', $exercise_favourite_type);
        }

        //Fitness Experience
        $fitness_experience_int = field_get_items('profile2', $profile2_info_1, 'field_fitness_experience_int');
        if (!empty($fitness_experience_int[0]['value'])) {
            $rows['fitness_experience'] = $fitness_experience_int[0]['value'];
        }
    }



    $profile2_info_2 = profile2_load_by_user($uid, 'user_info_2_squad_leader');

    if(!empty($profile2_info_2)){


        //Accomplishments
        $accomplishments = field_get_items('profile2', $profile2_info_2, 'field_accomplishments');
        if (!empty($accomplishments[0]['value'])) {
            $rows['accomplishments'] = $accomplishments[0]['value'];
        }

        //Classes Per Month
        $classes_per_month_term = field_get_items('profile2', $profile2_info_2, 'field_classes_per_month');
        $classes_per_month = taxonomy_term_load($classes_per_month_term[0]['tid']);

        if (!empty($classes_per_month->name)) {
            $rows['classes_per_month'] = $classes_per_month->name;
        }

        //Users Per Class
        $users_per_class_term = field_get_items('profile2', $profile2_info_2, 'field_users_per_class');
        $users_per_class = taxonomy_term_load($users_per_class_term[0]['tid']);

        if (!empty($users_per_class->name)) {
            $rows['users_per_class'] = $users_per_class->name;
        }

        //Reference 1 name
        $reference_1_name = field_get_items('profile2', $profile2_info_2, 'field_reference_1_name');
        if (!empty($reference_1_name[0]['value'])) {
            $rows['reference_1_name'] = $reference_1_name[0]['value'];
        }

        //Reference 1 email
        $reference_1_email = field_get_items('profile2', $profile2_info_2, 'field_reference_1_email');
        if (!empty($reference_1_email[0]['value'])) {
            $rows['reference_1_email'] = $reference_1_email[0]['value'];
        }

        //Reference 2 name
        $reference_2_name = field_get_items('profile2', $profile2_info_2, 'field_reference_2_name');
        if (!empty($reference_2_name[0]['value'])) {
            $rows['reference_2_name'] = $reference_2_name[0]['value'];
        }

        //Reference 2 email
        $reference_2_email = field_get_items('profile2', $profile2_info_2, 'field_reference_2_email');
        if (!empty($reference_2_email[0]['value'])) {
            $rows['reference_2_email'] = $reference_2_email[0]['value'];
        }

    }

    return $rows;
}

function user_without_profile_data ($uid){
        $user_without_profile[$uid] = db_select('users', 'u')
        ->fields('u', array('name', 'mail'))
        ->condition('uid', $uid)
        ->execute()
        ->fetchAllKeyed();

      $str=strripos(key($user_without_profile[$uid]), "_");
      $name = key($user_without_profile[$uid]);
      if($str == false){
        $user_name = $name;
      } else{
        $user_name=substr(key($user_without_profile[$uid]), 0, $str);
      }
      $rows['uid'] = $uid;
      $rows['name'] = $user_name;
      $rows['email'] = $user_without_profile[$uid][$name];

  return $rows;
}

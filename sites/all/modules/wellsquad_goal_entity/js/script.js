(function ($) {
    Drupal.behaviors.ajax_usersperday = {
        attach:function (context) {
            $('.day', '.calendar-holder').click (function() {
                var date = $(this).data('date');
                if(date.length == 0){
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: Drupal.settings.basePath+'ajax/usersperday',
                    data: {
                        date: date
                    },
                    success: function(data) {
                        var slickIndex = 0;
                        $('.squad-users-wrapper', '#block-wsg-squad_users_for').replaceWith(data);
                        if($('.goals-slider').find('.slick-active').length){
                            slickIndex = $('.slick-active', '.goals-slider').data('slick-index');
                        }

                        $('.squad-users-slider').slick({
                            infinite: true,
                            arrows: false,
                            speed: 0,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: true,
                            swipe: false,
                            initialSlide: slickIndex
                        });

                    }
                });

                return false;
            });
        }
    }
})(jQuery);
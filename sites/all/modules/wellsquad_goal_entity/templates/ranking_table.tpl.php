<div id="squad_ranking" class="ranking">
<?php if($data): ?>
	<?php if($title): ?><h3 class="block-title"><?php print $title; ?></h3><?php endif; ?>
	<div class="squad-ranking-type">
	<?php foreach($data as $key => $squad_users): ?>
		<span class="squad-ranking-head"><?php if( isset($squad_users['squad_title']) ) :?><?php print $squad_users['squad_title'];?> <?php print t('Squad');?><?php endif;?></span>
	<?php endforeach; ?>
	</div>
	<div class="squad-ranking">
		<?php foreach($data as $key => $squad_users): ?>
		<div>
			<ul class="ranking-list">
				<li><span class="weekly"><?php print t('Weekly Avg'); ?></span><span><?php print t('Monthly Total'); ?></span></li>
			<?php $i = 1; ?>
			<?php foreach($squad_users as $user): if( isset($user) && is_array($user) ) : extract($user); endif; ?>

				<li class="ranking-item">
						<span class="user-count"><?php print $i; ?></span>
						<div class="user-photo"><?php if(isset($user_info[$uid]->image)): ?><?php print render($user_info[$uid]->image); ?><?php endif; ?></div>
						<div class="heading-user"><?php if(isset($user_info[$uid]->name)): ?><h5><?php print $user_info[$uid]->name; ?> <?php print $user_info[$uid]->lastname ?></h5><?php endif; ?></div>
						<span class="weekly-avg"><?php if(isset($weekly_avg)): ?><?php print $weekly_avg; ?><?php endif; ?></span>
						<span class="monthly_avg"><?php if(isset($monthly_avg)): ?><?php print $monthly_avg; ?><?php endif; ?></span>
				</li>
				<?php $i++; ?>
			<?php endforeach;?>
			</ul>
		</div>
		<?php endforeach; ?>
	</div>
<?php else:?>
	<div class="squad-ranking">
		<?php if($title): ?><h3 class="block-title"><?php print $title; ?></h3><?php endif; ?>
	</div>
<?php print t('No Ranking data');?>
<?php endif;?>
</div>
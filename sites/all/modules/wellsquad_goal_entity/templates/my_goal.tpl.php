<?php if(!isset($data[0]['is_empty_current_month'] ) && count($data)):?>
	<div class="goals goals-carousel">
		<?php foreach($data as $goal): ?>
			<div class="goals-item">
				<div class="goals-preview">
					<?php echo render($goal['image_obj']); ?>
				</div>
				<section class="goals-description">
					<h2><span class="font--normal"><a href="/goals"><?php print t('My');?></span> <?php print t('goal');?></a></h2>
					<?php if(!empty($goal['squad_name'])): ?>
						<h4>
							<span class="font--normal"><strong><?php echo render($goal['squad_name']); ?></strong></span>
						</h4>
					<?php endif; ?>
					<div class="progress-circle"
						 data-dimension="174"
						 data-text="<?php echo $goal['percent_progress']; ?>%"
						 data-fontsize="40"
						 data-percent="<?php echo $goal['percent_progress']; ?>"
						 data-fgcolor="#1ce5d7"
						 data-bgcolor="transparent"
						 data-width="12"
						 data-bordersize="5"
						 data-fill="transparent"
						 data-animationstep="2">
					</div>
					<h3><?php echo $goal['temp_progress_sum'] . ' ' . _wsg_get_exercise_unit($goal['exercise_type_tid']); ?></h3>
					<span class="action"><?php print t('To GO!');?></span>
				</section>
			</div>
		<?php endforeach;?>
	</div>
<?php endif;?>

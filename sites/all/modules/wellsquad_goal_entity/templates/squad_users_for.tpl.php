<div class="squad-users-wrapper">
    <?php
//    $user_profile = profile2_load_by_user(54, 'user_info_2_squad_leader');
//    $user_photo = field_view_field('profile2', $user_profile, 'field_rating', array('label' => 'hidden'));
//    print drupal_render($user_photo);
//    print render($user_photo);

    ?>
    <h2 class="block-title"><?php print render($title_prefix); ?> <span class="date"><?php print render($title); ?></span></h2>
    <div class="squad-users-slider">
    <?php if(!empty($users_lists)): ?>
        <?php foreach($users_lists as $users_list): ?>
            <?php if(!empty($users_list)): ?>
                <div class="squad-users-list">
                    <ul class="users-list">
                        <?php foreach($users_list as $uid => $user_info): ?>
                            <li class="ranking-item">
                                <div class="user-photo"><?php print render($user_info->image); ?></div>
                                <div class="info-holder">
                                    <h5><?php print render($user_info->name); ?> <?php print render($user_info->lastname); ?></h5>
                                    <span class="view-profile-link"><?php print l(t('View Profile'), 'profile/' . $uid); ?></span>
                                    <span class="send-message"><?php print l(t('Send Message'), '/message'); ?></span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php else: ?>
                <div class="no-users">
                    <span class="no-usersmsg"><?php print t('Today no users'); ?></span>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="no-users">
            <span class="no-usersmsg"><?php print t('Today no users'); ?></span>
        </div>
    <?php endif; ?>
    </div>
</div>


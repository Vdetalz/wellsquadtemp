<?php
if(!empty($squads_users)){
    // CTools modal window
    ctools_include('modal');
    ctools_include('ajax');
    ctools_modal_add_js();
    drupal_add_js(array(
            'CToolsModal' => array(
                'modalSize' => array(
                    'type' => 'fixed',
                    'width' => 450,
                    'height' => 350,
                    'contentBottom' => 800),
            ),
        ), 'setting');
}
?>
<div class="goals-month">
    <div class="page-nav">
        <?php if(isset($month_links['prev'])):?>
            <?php print l(t("Slide Left"), '', array('external' => true, 'query' => $month_links['prev'], 'attributes' => array('class' => 'slick-prev')));?>
        <?php else:?>
          <span class="slick-prev disabled-month-link"><?php print t('Slide Left');?></span>
        <?php endif;?>

        <?php if(isset($month_links['next'])):?>
            <?php print l(t("Slide Right"), '', array('external' => true, 'query' => $month_links['next'], 'attributes' => array('class' => 'slick-next')));?>
        <?php else:?>
          <span class="slick-next disabled-month-link"><?php print t('Slide Right');?></span>
        <?php endif;?>
    </div>
    <div class="edit-goal">
        <?php print l(t("Edit Goal"), 'modal/goal/edit', array('attributes' => array('class' => 'ctools-use-modal goal-edit-link')));?>
    </div>
<?php if($data):?>

    <?php foreach($data as $key => $months): $i = 0;?>
        <?php foreach($months as $month => $goals):?>
            <div class="goal-month">
                <div class="gola-heding">
                    <div class="user-photo">
                        <?php echo render($user_info[$uid]->image);?>
                    </div>
                    <h2><span class="font--normal"><?php print t('My'); ?></span> <?php if(isset($is_team)) print t('team');?> <span class="font--bold"><?php if(isset($title)) print $title; else print t('fitness goals'); ?></span></h2>
                </div>
                <time datetime="<?php echo date("Y-m", strtotime($month . '-' . $key));?>">
                    <span><?php echo $month;?></span>
                    <?php echo $key;?>
                </time>
                <div class="goals-slider-<?php echo $key . "-" . $month;?> goals-slider">
                <?php foreach($goals as $goal):?>

                    <?php if(isset($goal['is_empty_current_month']) && $goal['is_empty_current_month']): ?>
                        <section class="goal-info">
                            <?php print t('You have no goals data');?>
                        </section>
                    <?php else:?>

                    <section class="goal-info">
                        <div class="goal-preview"><?php echo render($goal['image_fitness_obj']);?></div>
                        <div class="progress-circle"
                             data-dimension="280"
                             data-text="<?php echo $goal['percent_progress']; ?>%"
                             data-fontsize="60"
                             data-percent="<?php echo $goal['percent_progress']; ?>"
                             data-fgcolor="#1ce5d7"
                             data-bgcolor="transparent"
                             data-width="20"
                             data-bordersize="5"
                             data-fill="transparent"
                             data-animationstep="2">
                        </div>
                        <?php if ($goal['percent_progress'] != 100):?>
                            <h3><?php echo $goal['temp_progress_sum']; if(isset($user_role) && $user_role == 'leader'): echo ' ' . t('USERS'); else: echo ' ' . _wsg_get_exercise_unit($goal['exercise_type_tid']); endif; ?></h3>
                            <span class="action"><?php echo t('To GO!');?></span>
                        <?php else: ?>
                            <span class="action"><?php echo t('Goal achieved!'); ?></span>
                        <?php endif;?>
                    </section>

                <?php endif;?>

            <?php endforeach;?>

            </div>

            <?php if( !isset($goal['is_empty_current_month']) ): ?>

                <div class="thumbnail-holder">
                    <div class="goal-thumbnail-<?php echo $key . "-" . $month;?> goal-thumbnail">
                        <?php foreach($goals as $temp_goal):?>
                            <div <?php if($temp_goal['percent_progress'] >= 100): print 'class="goal-done"'; endif; ?> >
                                <div class="user-photo">
                                <?php if(isset($temp_goal['image'])):?>                         
                                    <?php echo render($temp_goal['image_preview_obj']);?>
                                <?php endif;?>

                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

            <?php endif;?>

           </div>
        <?php endforeach;?>

    <?php endforeach;?>

<?php endif;?>

</div>



<?php if(!empty($user_info)): ?>
    <?php
        // CTools modal window
        ctools_include('modal');
        ctools_include('ajax');
        ctools_modal_add_js();
        drupal_add_js(array(
                'CToolsModal' => array(
                    'modalSize' => array(
                        'type' => 'fixed',
                        'width' => 450,
                        'height' => 350,
                        'contentBottom' => 800),
                ),
            ), 'setting');
    ?>

    <section class="profile-section profile-section2 border-bottom bg--dark">
        <h2 class="heading--styled">
            <?php print t('Profile'); ?>
        </h2>
        <div class="profile-info">
            <div class="profile-cart">
            <?php if(!empty($user_info->gender)): ?>
                <div class="photo-wrap">
                    <div class="profile-photo">
                        <?php print render($user_info->photo); ?>
                    </div>
                        <?php print ctools_modal_text_button($user_info->rating, $popup . '/profile/' . $user_info->uid, t('rate'), 'rating');?>
                    <div id="send-email" class="send-email">
                        <div id="send-email" class="send-email"><?php print l('Email link', "modal/nojs/message/" . $user_info->uid, array('attributes' => array('class' => 'ctools-use-modal'))); ?></div>
                    </div>
                </div>
            <?php endif; ?>
            <section class="param-holder">
                <h3><?php print render($user_info->name); ?></h3>
                <?php if(!empty($user_info->gender)):?>
                    <div class="gender param-row">
                        <span><?php print t('Gender: ');?></span><?php print render($user_info->gender); ?>
                    </div>
                <?php endif; ?>
               
			   <?php if(!empty($user_info->profession)):?>
				  <div class="url param-row">
					<span><?php print t('Profession: ');?></span><?php print render($user_info->profession); ?>
				  </div>
				<?php endif; ?>
						
                <?php if(!empty($user_info->location)):?>
                    <div class="location param-row">
                        <span><?php print t('Location: ');?></span><?php print render($user_info->location); ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($user_info->url)):?>
                    <div class="url param-row">
                        <span><?php print t('URL: ');?></span><?php print render($user_info->url); ?>
                    </div>
                <?php endif; ?>
            </section>
            </div>
            <article class="profile-article">
            <?php if(!empty($user_info->description)):?>
                <h3><?php print t('Choose from our selection of experts, we call Squad Leaders, to jump start your results!'); ?></h3>
                <div class="profile-desc">
                    <?php print render($user_info->description); ?>
                </div>
            <?php endif; ?>
            </article>
            <div class="match">
                <?php print render($user_info->match_link); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if(!empty($squads)):?>
    <section class="profile-section border-bottom">
        <h2 class="squads-title"><strong><?php print t('Classes'); ?></strong></h2>
        <div class="squads-wrapper can-buy">
            <?php foreach($squads as $nid => $squad): ?>
                <div id="squad-<?php print $nid; ?>" class="squod-item">
                    <div class="squod-block">
                        <?php if(!empty($squad->image)): ?>
                            <div class="squod-preview">
                                <div class="squod-image"><?php print render($squad->image); ?></div>
                                <?php if(!empty($squad->user_number)): ?>
                                    <div class="squod-number">
                                        <span class="user-number"><?php print $squad->user_number; ?></span>
                                        <span>users</span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <h3 class="squod-title"><?php print $squad->title; ?></h3>
                        <ul class="description-list">
                            <?php if(!empty($squad->tagline)): ?>
                                <li>
                                    <h5 class="squod-tagline"><?php print $squad->tagline; ?></h5>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->description)): ?>
                                <li>
                                    <h5><?php print t('Description: ');?></h5>
                                    <div class="squod-description"><?php print $squad->description; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->length)): ?>
                                <li>
                                    <div class="squod-length">
                                        <h5><?php print t('Length of time: ');?></h5>
                                        <?php print $squad->length; ?>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->schedule)): ?>
                                <li>
                                    <h5><?php print t('Schedule: ');?></h5>
                                    <div class="squod-schedule"><?php print $squad->schedule; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->squad_offers)): ?>
                                <li>
                                    <h5><?php print t('Squad Offers: ');?></h5>
                                    <div class="squod-squad-offers"><?php print $squad->squad_offers; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->location)): ?>
                                <li>
                                    <h5><?php print t('Location: ');?></h5>
                                    <div class="squod-location"><?php print $squad->location; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->price)): ?>
                                <li>
                                    <h5><?php print t('Price: ');?></h5>
                                    <div class="squod-price"><?php print $squad->price; ?></div>
                                </li>
                            <?php endif; ?>

                            <?php if(!empty($squad->openings)): ?>
                                <li>
                                    <h5><?php print t('Openings: ');?></h5>
                                    <div class="squod-openings"><?php print $squad->openings; ?></div>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="buy">
                            <?php print render($squad->buy_link); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
<?php if(!empty($profiles)): ?>
    <section class="profile-section profile-section2 border-bottom bg--dark">
        <?php foreach($profiles as $profile): ?>
            <div class="profile-info">
                <div class="profile-cart">
                    <div class="photo-wrap">
                        <div class="profile-photo view-profile">
                            <?php print render($profile->photo); ?>
							<?php print l(t('View profile to connect'), $profile->profile_link, array('attributes' => array('class' => array('profile-link')))); ?>
                        </div>
                    </div>
                    <section class="param-holder">
                        <h3><?php print render($profile->name); ?></h3>

                        <?php if(!empty($profile->gender)):?>
                            <div class="gender param-row">
                                <span><?php print t('Gender: ');?></span><a href="#"><?php print render($profile->gender); ?></a>
                            </div>
                        <?php endif; ?>

                        <?php if(!empty($profile->age)):?>
                            <div class="age param-row">
                                <span><?php print t('Age: ');?></span><?php print render($profile->age); ?>
                            </div>
                        <?php endif; ?>

						 <?php if(!empty($profile->profession)):?>
                          <div class="url param-row">
                            <span><?php print t('Profession: ');?></span><?php print render($profile->profession); ?>
                          </div>
                        <?php endif; ?>

                        <?php if(!empty($profile->location)):?>
                            <div class="location param-row">
                                <span><?php print t('Location: ');?></span><?php print render($profile->location); ?>
                            </div>
                        <?php endif; ?>
                        <?php if(!empty($profile->url)):?>
                            <div class="url param-row">
                                <span><?php print t('URL: ');?></span><?php print render($profile->url); ?>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
                <div class="more">
                    <?php print render($profile->edit_link); ?>
                </div>
            </div>
        <?php endforeach; ?>
        <?php print render($pager); ?>
    </section>
<?php else: ?>
    <span class="not-found--text"><?php print t('Sorry, but we could not find anyone.'); ?></span>
<?php endif; ?>
<div class="calendar-slider">
    <?php foreach ($calendars as $calendar) : ?>
        <div class="calendar-holder">
            <table class="calendar">
                <thead>
                <tr class="navigation">
                    <th colspan="7" class="current-month"><?php echo $calendar->month(); ?> <?php echo $calendar->year; ?></th>
                </tr>
                <tr class="weekdays">
                    <?php foreach ($calendar->days(3) as $day): ?>
                        <th><?php echo $day ?></th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($calendar->weeks() as $week): ?>
                    <tr>

                        <?php foreach ($week as $day): ?>
                            <?php
                            list($number, $current, $data) = $day;
                            $classes = array();
                            $output  = '';

                            if (is_array($data))
                            {
                                $classes = $data['classes'];
                                $title   = $data['title'];
                                $output  = empty($data['output']) || !isset($data['output']) ? '' :
                                    '<ul class="output">
                                        <li>' .
                                            implode('</li><li>', $data['output'])
                                    . '</li>
                                </ul>';
                            }
                            ?>
                            <td class="day <?php echo implode(' ', $classes) ?>" data-date="<?php print strtotime($number . '-' . $calendar->month . '-' . $calendar->year); ?>">

                                <?php if( empty($data['output']) ):?>
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                <?php else:?>
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                <?php endif;?>

                                <div class="day-content">
                                    <?php echo $output ?>
                                </div>
                            </td>
                        <?php endforeach ?>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    <?php endforeach;?>
</div>
<?php 
ctools_include('modal');
ctools_include('ajax');
ctools_modal_add_js();
drupal_add_js(array(
        'CToolsModal' => array(
            'modalSize' => array(
                'type' => 'fixed',
                'width' => 450,
                'height' => 350,
                'contentBottom' => 800),
        ),
    ), 'setting');
?>
<div class="calendar-slider">
	<?php if($data): ?>
		<?php foreach ($data as $key => $calendar) : ?>
				<div class="calendar-holder">
						<?php if($is_user):?>
								<span><?php print t('Input your Activity Using the Calendar'); ?></span>
						<?php endif;?>
						<table class="calendar">
								<thead>
										<tr class="navigation">
												<th colspan="7" class="current-month"><?php echo $calendar->month(); ?> <?php echo $calendar->year; ?></th>
										</tr>
										<tr class="weekdays">
												<?php foreach ($calendar->days(3) as $day): ?>
														<th><?php echo $day ?></th>
												<?php endforeach ?>
										</tr>
								</thead>
								<tbody>
								<?php foreach ($calendar->weeks() as $week): ?>
										<tr>
												<?php foreach ($week as $day): ?>
														<?php
														list($number, $current, $data) = $day;

														$classes = array();
														$output  = '';

														if (is_array($data))
														{
																$classes = $data['classes'];
																$title   = $data['title'];
																$output  = empty($data['output'][0]['progress']) || !isset($data['output'][0]['progress']) ? '' :
																		'<ul class="output">
																				<li>
																						<a href="/calendar_event/' . $data['output'][0]['id'] . '" class="ctools-use-modal" id="event-' . $data['output'][0]['id'] . '">'. $data['output'][0]['progress'] .'</a>
																				</li>
																		</ul>';
														}
														?>
														<td class="day <?php echo implode(' ', $classes) ?>">

															<?php $timestamp = strtotime($calendar->year . '-' . $calendar->month . '-' . $number);
															$config = $calendar->getConfig();?>

															<?php if(in_array('prev-next', $classes) || empty($config['settings']['goal_id'])): ?>
															<span class="date" title="<?php echo implode(' / ', $title) ?>"><?php print $number;?></span>
															<?php else: ?>
																<span class="date" title="<?php echo implode(' / ', $title) ?>"><?php print l($number, 'calendar_set_activity/' . $config['settings']['goal_id'] . '/' . $timestamp, array(
																		'attributes' => array('class' => 'ctools-use-modal')
																	)); ?>
																</span>
															<?php endif; ?>

															<div class="day-content">
																		<?php echo $output ?>
																</div>
														</td>
												<?php endforeach ?>
										</tr>
								<?php endforeach ?>
								</tbody>
						</table>
				</div>
		<?php endforeach;?>
	<?php else: ?>
		<?php print t('No Goals data'); ?>
	<?php endif;?>
</div>
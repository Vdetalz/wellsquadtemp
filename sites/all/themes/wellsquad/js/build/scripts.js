(function() {
  window.fbAsyncInit = function() {
    FB.init({
      appId: Drupal.settings.wellsquad_connections.fbAPIKey,
      cookie: true,
      status: true,
      xfbml: true
    });
  }

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
}());


jQuery(document).ready(function($) {
	$(window).on('load resize', function(){
		menuVisibility();
	});

    $(document).on('click', '.facebook-invite-link', function() {

    FB.ui({
      method: 'apprequests',
      message: 'Wellsquad App'
    });
  })

  	//};



	$(function() {

		if(jQuery('.leaf.messages a') && 'wellsquad_configurations' in Drupal.settings) {
            var message_count = Drupal.settings.wellsquad_configurations.messages_count;
            if(!(typeof message_count == "undefined")) {
                jQuery('.leaf.messages a').html(jQuery('.leaf.messages a').html() + ' <span>' + message_count + '</span>');
            }
		}
		jcf.setOptions('Select', {
			"wrapNative": false,
			"wrapNativeOnMobile": false,
			"fakeDropInBody": false,
			"useCustomScroll": false
		});
		jcf.replaceAll();
		initAccordion();
		initTabs();
		$("ul.messages li:first-child a").trigger('click');
		$('.progress-circle').circliful();

		jQuery(document).ajaxComplete(function(event, xhr, settings) {
			jcf.replaceAll();
		});

	});

	$('.squads-wrapper').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});

    if($('.profile-section-connect').length != 0){
        $('.squads-wrapper-profile').slick({
            dots: true,
            infinite: true,
            speed: 0,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true,
            asNavFor: '.squads-list'
        });

        $('.squads-list').slick({
            dots: true,
            arrows: false,
            infinite: true,
            speed: 0,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true,
            swipe: false,
            asNavFor: '.squads-wrapper-profile'
        });

    }else{
        var slickObj = {
            dots: true,
            infinite: true,
            speed: 0,
            initialSlide: 0,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true
        }


        var squad = getQueryVariable('squad');
        if(squad.length){
            var slideIndex = $('#' + squad).index(".squod-item");
            slickObj.initialSlide = slideIndex;
        }

        $('.squads-wrapper-profile').slick(slickObj);
    }

	$('.squads-wrapper-profile_user').slick({
		dots: true,
		infinite: true,
		speed: 0,
		slidesToShow: 1,
		adaptiveHeight: true,
		fade: true,
		asNavFor: '.leader-list'
	});
	$('.leader-list').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed: 0,
		slidesToShow: 1,
		adaptiveHeight: true,
		fade: true,
		swipe: false,
		asNavFor: '.squads-wrapper-profile_user'
	});

	$('.users-wrapper').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});

	$('.users-carousel').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.subcategory-list'
	});

	$('.goals-carousel').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
	});

	$('.squod-item').matchHeight();
	$('.block-views').matchHeight();

	$('#block-views-small_profile_info-block, #block-wellsquad_configurations-my_match').matchHeight();

	$('.subcategory-list').slick({
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: '.users-carousel'
	});
	$('.squad-ranking').slick({
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		swipe: false
	});
	$('.squad-ranking-type').slick({
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		swipe: false
	});
	$('.goals-slider').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: false,
				dots: false
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});
	$('.goal-thumbnail').slick({
		dots: false,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		swipe: false
	});
	$('.calendar-slider').slick({
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		swipe: false
	});
	$('.squad-users-slider').slick({
		infinite: true,
		arrows: false,
		speed: 0,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		swipe: false
	});
	$('.goals-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$('.squad-ranking').slick('slickGoTo', nextSlide);
		$('.squad-ranking-type').slick('slickGoTo', nextSlide);
		$('.goal-thumbnail').slick('slickGoTo', nextSlide);
		$('.calendar-slider').slick('slickGoTo', nextSlide);
		$('.squad-users-slider').slick('slickGoTo', nextSlide);
		currentPagerShow(nextSlide);
	});
	$('.goal-thumbnail').on('click', '.slick-slide', function () {
		$('.goals-slider').slick('slickGoTo', $(this).data('slick-index'));
	});
	$(".profile-link2").click(function(event) {
		window.location = $(".profile-link2 + .btn-wrap a").attr('href');
	});
	currentPagerShow();
	// remove extra active from MiddleSlider 'js-ot-pager'
	function currentPagerShow(elem) {
		var activeBigSlide = jQuery.type( elem ) === "number" ? elem : $('.goals-slider').find('.slick-active').data('slick-index');
		$('.goal-thumbnail .slick-slide').each(function () {
			($(this).data('slick-index') == activeBigSlide) ? $(this).addClass('slick-active') : $(this).removeClass('slick-active');
		})
	}
	haveSidebar();
	fullMsg();
	initSearch();
});

function getQueryVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

function initSearch(){
	var search_btn = jQuery('.search-icon');
	var search = jQuery('#block-wps-profile_search');
	search_btn.on('click', function(event) {
		event.preventDefault();
		search.slideToggle('slow');
	});
}

// function for show or hide full message and reply form
function fullMsg(){
	var msg = jQuery('.inbox-messages .messages li');
	var replyForm = jQuery('.inbox-messages .messages .reply-form').slideUp(0);

    jQuery('#wellsquad-configurations-reply-form').on('click', function(event) {
        return false;
    });

    jQuery('#wellsquad-configurations-reply-form :submit').on('click', function(event) {
        jQuery('#wellsquad-configurations-reply-form').submit();
    });

    jQuery('.about-link').on('click', function(event) {
        window.open(
            jQuery(this).attr('href'),
            '_blank'
        );
        event.stopPropagation();
    });

    jQuery('.full-message').on('click', function(event) {
        event.stopPropagation();
    });

	msg.on('click', function(event) {

        var element = jQuery(this).data('user-id');
        if(element && !jQuery(this).hasClass('show-form')) {
            var form = jQuery('#wellsquad-configurations-reply-form');
            jQuery(this).find('.reply-form').append(form);
            var userId = jQuery(this).data('user-id');
            var subject = jQuery(this).find('.short-message').html().trim();
            jQuery('#message_user_id').val(userId);
            jQuery('#message_subject').val(subject);
        }

		event.preventDefault();
        var target = jQuery(this);
        target.toggleClass('show-form');
        var msgInner = target.find('.inbox .reply-form') || undefined;
        msgInner.slideToggle('slow');

	});
}

// function for menu on mobile
function menuVisibility(){
	var windowWidth = jQuery(window).width();
	if(windowWidth > 1204){
		jQuery('#sidebar-first').slideDown(0);
	} else {
		jQuery('#sidebar-first').slideUp(0);
	}
	jQuery('#sidebar-first .close-btn').on('click', function(event) {
		event.preventDefault();
		jQuery('#sidebar-first').slideUp();
	});
	jQuery('#content .open-btn').on('click', function(event) {
		event.preventDefault();
		jQuery('#sidebar-first').slideDown();
	});
}

// add class on body when aside
function haveSidebar(){
	if(jQuery('aside').length != 0){
		jQuery('body').addClass('has-sidebar');
	}
}

// content tabs init
function initTabs() {
	jQuery('ul.tabset').contentTabs({
		tabLinks: 'a'
	});
}

// accordion menu init
function initAccordion() {
	jQuery('div.accordion').slideAccordion({
		opener: '.opener',
		slider: 'div.slide',
		animSpeed: 300
	});
}

/*
 * jQuery Accordion plugin
 */
;(function($){
	$.fn.slideAccordion = function(opt){
		// default options
		var options = $.extend({
			addClassBeforeAnimation: false,
			activeClass:'active',
			opener:'.opener',
			slider:'.slide',
			animSpeed: 300,
			collapsible:true,
			event:'click'
		},opt);

		return this.each(function(){
			// options
			var accordion = $(this);
			var items = accordion.find(':has('+options.slider+')');

			items.each(function(){
				var item = $(this);
				var opener = item.find(options.opener);
				var slider = item.find(options.slider);
				opener.bind(options.event, function(e){
					if(!slider.is(':animated')) {
						if(item.hasClass(options.activeClass)) {
							if(options.collapsible) {
								slider.slideUp(options.animSpeed, function(){
									hideSlide(slider);
									item.removeClass(options.activeClass);
								});
							}
						} else {
							// show active
							var levelItems = item.siblings('.'+options.activeClass);
							var sliderElements = levelItems.find(options.slider);
							item.addClass(options.activeClass);
							showSlide(slider).hide().slideDown(options.animSpeed);

							// collapse others
							sliderElements.slideUp(options.animSpeed, function(){
								levelItems.removeClass(options.activeClass);
								hideSlide(sliderElements);
							});
						}
					}
					e.preventDefault();
				});
				if(item.hasClass(options.activeClass)) showSlide(slider); else hideSlide(slider);
			});
		});
	};

	// accordion slide visibility
	var showSlide = function(slide) {
		return slide.css({position:'', top: '', left: '', width: '' });
	};
	var hideSlide = function(slide) {
		return slide.show().css({position:'absolute', top: -9999, left: -9999, width: slide.width() });
	};
}(jQuery));

/*
 * jQuery Tabs plugin
 */
;(function($){
	$.fn.contentTabs = function(o){
		// default options
		var options = $.extend({
			activeClass:'active',
			addToParent:false,
			autoHeight:false,
			autoRotate:false,
			checkHash:false,
			animSpeed:400,
			switchTime:3000,
			effect: 'none', // "fade", "slide"
			tabLinks:'a',
			attrib:'href',
			event:'click'
		},o);

		return this.each(function(){
			var tabset = $(this), tabs = $();
			var tabLinks = tabset.find(options.tabLinks);
			var tabLinksParents = tabLinks.parent();
			var prevActiveLink = tabLinks.eq(0), currentTab, animating;
			var tabHolder;

			// handle location hash
			if(options.checkHash && tabLinks.filter('[' + options.attrib + '="' + location.hash + '"]').length) {
				(options.addToParent ? tabLinksParents : tabLinks).removeClass(options.activeClass);
				setTimeout(function() {
					window.scrollTo(0,0);
				},1);
			}

			// init tabLinks
			tabLinks.each(function(){
				var link = $(this);
				var href = link.attr(options.attrib);
				var parent = link.parent();
				href = href.substr(href.lastIndexOf('#'));

				// get elements
				var tab = $(href);
				tabs = tabs.add(tab);
				link.data('cparent', parent);
				link.data('ctab', tab);

				// find tab holder
				if(!tabHolder && tab.length) {
					tabHolder = tab.parent();
				}

				// show only active tab
				var classOwner = options.addToParent ? parent : link;
				if(classOwner.hasClass(options.activeClass) || (options.checkHash && location.hash === href)) {
					classOwner.addClass(options.activeClass);
					prevActiveLink = link; currentTab = tab;
					tab.removeClass(tabHiddenClass).width('');
					contentTabsEffect[options.effect].show({tab:tab, fast:true});
				} else {
					var tabWidth = tab.width();
					if(tabWidth) {
						tab.width(tabWidth);
					}
					tab.addClass(tabHiddenClass);
				}

				// event handler
				link.bind(options.event, function(e){
					if(link != prevActiveLink && !animating) {
						switchTab(prevActiveLink, link);
						prevActiveLink = link;
					}
				});
				if(options.attrib === 'href') {
					link.bind('click', function(e){
						e.preventDefault();
					});
				}
			});

			// tab switch function
			function switchTab(oldLink, newLink) {
				$('#message_user_id').val(newLink.data('user-id'));
				animating = true;
				var oldTab = oldLink.data('ctab');
				var newTab = newLink.data('ctab');
				prevActiveLink = newLink;
				currentTab = newTab;

				// refresh pagination links
				(options.addToParent ? tabLinksParents : tabLinks).removeClass(options.activeClass);
				(options.addToParent ? newLink.data('cparent') : newLink).addClass(options.activeClass);

				// hide old tab
				resizeHolder(oldTab, true);
				contentTabsEffect[options.effect].hide({
					speed: options.animSpeed,
					tab:oldTab,
					complete: function() {
						// show current tab
						resizeHolder(newTab.removeClass(tabHiddenClass).width(''));
						contentTabsEffect[options.effect].show({
							speed: options.animSpeed,
							tab:newTab,
							complete: function() {
								if(!oldTab.is(newTab)) {
									oldTab.width(oldTab.width()).addClass(tabHiddenClass);
								}
								animating = false;
								resizeHolder(newTab, false);
								autoRotate();
							}
						});
					}
				});
			}

			// holder auto height
			function resizeHolder(block, state) {
				var curBlock = block && block.length ? block : currentTab;
				if(options.autoHeight && curBlock) {
					tabHolder.stop();
					if(state === false) {
						tabHolder.css({height:''});
					} else {
						var origStyles = curBlock.attr('style');
						curBlock.show().css({width:curBlock.width()});
						var tabHeight = curBlock.outerHeight(true);
						if(!origStyles) curBlock.removeAttr('style'); else curBlock.attr('style', origStyles);
						if(state === true) {
							tabHolder.css({height: tabHeight});
						} else {
							tabHolder.animate({height: tabHeight}, {duration: options.animSpeed});
						}
					}
				}
			}
			if(options.autoHeight) {
				$(window).bind('resize orientationchange', function(){
					tabs.not(currentTab).removeClass(tabHiddenClass).show().each(function(){
						var tab = jQuery(this), tabWidth = tab.css({width:''}).width();
						if(tabWidth) {
							tab.width(tabWidth);
						}
					}).hide().addClass(tabHiddenClass);

					resizeHolder(currentTab, false);
				});
			}



			// autorotation handling
			var rotationTimer;
			function nextTab() {
				var activeItem = (options.addToParent ? tabLinksParents : tabLinks).filter('.' + options.activeClass);
				var activeIndex = (options.addToParent ? tabLinksParents : tabLinks).index(activeItem);
				var newLink = tabLinks.eq(activeIndex < tabLinks.length - 1 ? activeIndex + 1 : 0);
				prevActiveLink = tabLinks.eq(activeIndex);
				switchTab(prevActiveLink, newLink);
			}
			function autoRotate() {
				if(options.autoRotate && tabLinks.length > 1) {
					clearTimeout(rotationTimer);
					rotationTimer = setTimeout(function() {
						if(!animating) {
							nextTab();
						} else {
							autoRotate();
						}
					}, options.switchTime);
				}
			}
			autoRotate();
		});
	};

	// add stylesheet for tabs on DOMReady
	var tabHiddenClass = 'js-tab-hidden';
	$(function() {
		var tabStyleSheet = $('<style type="text/css">')[0];
		var tabStyleRule = '.'+tabHiddenClass;
		tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
		if (tabStyleSheet.styleSheet) {
			tabStyleSheet.styleSheet.cssText = tabStyleRule;
		} else {
			tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
		}
		$('head').append(tabStyleSheet);
	});

	// tab switch effects
	var contentTabsEffect = {
		none: {
			show: function(o) {
				o.tab.css({display:'block'});
				if(o.complete) o.complete();
			},
			hide: function(o) {
				o.tab.css({display:'none'});
				if(o.complete) o.complete();
			}
		},
		fade: {
			show: function(o) {
				if(o.fast) o.speed = 1;
				o.tab.fadeIn(o.speed);
				if(o.complete) setTimeout(o.complete, o.speed);
			},
			hide: function(o) {
				if(o.fast) o.speed = 1;
				o.tab.fadeOut(o.speed);
				if(o.complete) setTimeout(o.complete, o.speed);
			}
		},
		slide: {
			show: function(o) {
				var tabHeight = o.tab.show().css({width:o.tab.width()}).outerHeight(true);
				var tmpWrap = $('<div class="effect-div">').insertBefore(o.tab).append(o.tab);
				tmpWrap.css({width:'100%', overflow:'hidden', position:'relative'}); o.tab.css({marginTop:-tabHeight,display:'block'});
				if(o.fast) o.speed = 1;
				o.tab.animate({marginTop: 0}, {duration: o.speed, complete: function(){
					o.tab.css({marginTop: '', width: ''}).insertBefore(tmpWrap);
					tmpWrap.remove();
					if(o.complete) o.complete();
				}});
			},
			hide: function(o) {
				var tabHeight = o.tab.show().css({width:o.tab.width()}).outerHeight(true);
				var tmpWrap = $('<div class="effect-div">').insertBefore(o.tab).append(o.tab);
				tmpWrap.css({width:'100%', overflow:'hidden', position:'relative'});

				if(o.fast) o.speed = 1;
				o.tab.animate({marginTop: -tabHeight}, {duration: o.speed, complete: function(){
					o.tab.css({display:'none', marginTop:'', width:''}).insertBefore(tmpWrap);
					tmpWrap.remove();
					if(o.complete) o.complete();
				}});
			}
		}
	};
}(jQuery));

(function($){
	$(function(){
		$('.js-showMore').on('click', function(){
			$(this).closest('section').find('.js-moreBlock').addClass('is-open');
		});
		$('.js-moreClose').on('click', function(){
			$(this).closest('.js-moreBlock').removeClass('is-open');
		});
	});
})(jQuery)
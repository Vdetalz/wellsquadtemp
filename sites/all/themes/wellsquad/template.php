<?php

/**
 * Here we override the default HTML output of drupal.
 * refer to https://drupal.org/node/457740
 */

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('clear_registry')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

// Add Zen Tabs styles
if (theme_get_setting('wellsquad_tabs')) {
  drupal_add_css( drupal_get_path('theme', 'basic') .'/css/tabs.css');
}

function wellsquad_theme(){
    return array(
        'profile2_edit_user_info_1_form' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates',
            'arguments' => array('form' => NULL),
            'render element' => 'form',
            'template' => 'user-profile-edit',
        ),
        'profile2_edit_user_info_2_form' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates',
            'arguments' => array('form' => NULL),
            'render element' => 'form',
            'template' => 'user-profile-edit-2',
        ),
        'profile2_edit_user_info_1_squad_leader_form' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates',
            'arguments' => array('form' => NULL),
            'render element' => 'form',
            'template' => 'user-profile-squad-leader-edit-1',
        ),
        'profile2_edit_user_info_2_squad_leader_form' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates',
            'arguments' => array('form' => NULL),
            'render element' => 'form',
            'template' => 'user-profile-squad-leader-edit-2',
        ),
        'wellsquad_uc_cart_checkout_pane' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates/uc/',
            'arguments' => array('data' => NULL),
            'template' => 'uc_cart_checkout_pane',
        ),
        'wellsquad_uc_checkout_form' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates/uc/',
            'arguments' => array('form' => NULL),
            'template' => 'uc_checkout_form',
        ),
        'not_found' => array(
            'path' => drupal_get_path('theme', 'wellsquad') . '/templates/',
            'template' => 'page--404',
        ),
    );
}

function wellsquad_preprocess_html(&$vars) {
  global $user, $language;

  // Add role name classes (to allow css based show for admin/hidden from user)
  foreach ($user->roles as $role){
    $vars['classes_array'][] = 'role-' . wellsquad_id_safe($role);
  }

  //need for normal structure profile pages

  if(in_array(arg(0), array('profile-user', 'profile-squad-leader')) && !arg(2)){
      $vars['classes_array'][] = 'page-profile';
  }

  // HTML Attributes
  // Use a proper attributes array for the html attributes.
  $vars['html_attributes'] = array();
  $vars['html_attributes']['lang'][] = $language->language;
  $vars['html_attributes']['dir'][] = $language->dir;

  // Convert RDF Namespaces into structured data using drupal_attributes.
  $vars['rdf_namespaces'] = array();
  if (function_exists('rdf_get_namespaces')) {
    foreach (rdf_get_namespaces() as $prefix => $uri) {
      $prefixes[] = $prefix . ': ' . $uri;
    }
    $vars['rdf_namespaces']['prefix'] = implode(' ', $prefixes);
  }

  // Flatten the HTML attributes and RDF namespaces arrays.
  $vars['html_attributes'] = drupal_attributes($vars['html_attributes']);
  $vars['rdf_namespaces'] = drupal_attributes($vars['rdf_namespaces']);

  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $vars['classes_array'][] = 'with-subnav';
    $vars['classes_array'][] = wellsquad_id_safe('page-'. $path);
    $vars['classes_array'][] = wellsquad_id_safe('section-'. $section);

    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          // Remove 'section-node'
          array_pop( $vars['classes_array'] );
        }
        // Add 'section-node-add'
        $vars['classes_array'][] = 'section-node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          // Remove 'section-node'
          array_pop( $vars['classes_array']);
        }
        // Add 'section-node-edit' or 'section-node-delete'
        $vars['classes_array'][] = 'section-node-'. arg(2);
      }


    }
  }
  //for normal un-themed edit pages
  if ((arg(0) == 'node') && (arg(2) == 'edit')) {
    $vars['template_files'][] =  'page';
  }

  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/build/scripts.js', array('scope' => 'footer'));

  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jcf/jcf.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jcf/jcf.checkbox.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jcf/jcf.file.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jcf/jcf.radio.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jcf/jcf.select.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/slick.min.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jquery.circliful.min.js');
  drupal_add_js(drupal_get_path('theme', 'wellsquad') . '/js/source/jquery.matchHeight-min.js');
  drupal_add_js('https://connect.facebook.net/en_US/all.js');

  drupal_add_js(array('wellsquad_connections' => array('fbAPIKey' => variable_get('_facebook_api_key'))), 'setting');
  //drupal_add_js(array('wellsquad_connections' => array('fbAPISecretKey' => variable_get('_facebook_api_secret'))), 'setting');

  // Add IE classes.
  if (theme_get_setting('wellsquad_ie_enabled')) {
    $wellsquad_ie_enabled_versions = theme_get_setting('wellsquad_ie_enabled_versions');
    if (in_array('ie8', $wellsquad_ie_enabled_versions, TRUE)) {
      drupal_add_css(path_to_theme() . '/css/ie8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
      drupal_add_js(path_to_theme() . '/js/build/selectivizr-min.js');
    }
    if (in_array('ie9', $wellsquad_ie_enabled_versions, TRUE)) {
      drupal_add_css(path_to_theme() . '/css/ie9.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 9', '!IE' => FALSE), 'preprocess' => FALSE));
    }
    if (in_array('ie10', $wellsquad_ie_enabled_versions, TRUE)) {
      drupal_add_css(path_to_theme() . '/css/ie10.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 10', '!IE' => FALSE), 'preprocess' => FALSE));
    }
  }

}

function wellsquad_preprocess_page(&$vars, $hook) {
    $types = array('about_us', 'faq', 'how_it_works', 'privacy', 'terms'  );
    if (isset($vars['node']) && in_array($vars['node']->type, $types)) {

        $vars['header_class'] = 'header--transparent header--darck';
        drupal_set_title('');

        if(file_create_url(theme_get_setting('sublogo_path'))){
            $vars['logo'] = file_create_url(theme_get_setting('sublogo_path'));
        }
    }

    if (arg(0) === 'msg') {
        $vars['title'] = '<span>' . t('MESSAGE') . '</span> ' . t('CENTER');
    }

    if( drupal_is_front_page() ){

        if( $promotion_items = field_get_items('node', $vars['node'], 'field_promotion') ){
            foreach($promotion_items as $promo_item){
                if ($fc_items = field_collection_field_get_entity($promo_item)){

                    $vars['node']->field_promotion_data[] = array(
                        'background-image' => field_get_items('field_collection_item', $fc_items, 'field_background_image'),
                        'content' => field_get_items('field_collection_item', $fc_items, 'field_content'),
                    );
                }
            }
        }
    }

    //redirect from user login/register page
    if(in_array(current_path(), array('user/login', 'user/register', 'cart'))){
        drupal_goto('<front>');
    }

    if (isset($vars['node_title'])) {
      $vars['title'] = $vars['node_title'];
    }

    // Adding classes whether #navigation is here or not
    if (!empty($vars['main_menu']) or !empty($vars['sub_menu'])) {
      $vars['classes_array'][] = 'with-navigation';
    }

    if (!empty($vars['secondary_menu'])) {
      $vars['classes_array'][] = 'with-subnav';
    }

    // Add first/last classes to node listings about to be rendered.
    if (isset($vars['page']['content']['system_main']['nodes'])) {
      // All nids about to be loaded (without the #sorted attribute).
      $nids = element_children($vars['page']['content']['system_main']['nodes']);
      // Only add first/last classes if there is more than 1 node being rendered.
      if (count($nids) > 1) {
        $first_nid = reset($nids);
        $last_nid = end($nids);
        $first_node = $vars['page']['content']['system_main']['nodes'][$first_nid]['#node'];
        $first_node->classes_array = array('first');
        $last_node = $vars['page']['content']['system_main']['nodes'][$last_nid]['#node'];
        $last_node->classes_array = array('last');
      }
  }

  // Allow page override template suggestions based on node content type.
  if (isset($vars['node']->type) && isset($vars['node']->nid)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
    $vars['theme_hook_suggestions'][] = "page__node__" . $vars['node']->nid;
  }

  // Add cookie for Cancel button on the checkout
  $profiles = array(
	'profile-squad-leader',
    'profile-user',
  );

  if ((arg(0) == 'profile' && is_numeric(arg(1))) || in_array(arg(0), $profiles)) {
      $url = current_path();
      user_cookie_save(array('cancel_checkout_redirect' => $url));
  }

}

function wellsquad_preprocess_node(&$vars) {
  // Add a striping class.
  $vars['classes_array'][] = 'node-' . $vars['zebra'];

  // Add $unpublished variable.
  $vars['unpublished'] = (!$vars['status']) ? TRUE : FALSE;

  // Merge first/last class (from wellsquad_preprocess_page) into classes array of current node object.
    $node = $vars['node'];
    if (!empty($node->classes_array)) {
    $vars['classes_array'] = array_merge($vars['classes_array'], $node->classes_array);
    }

}

function wellsquad_preprocess_block(&$vars, $hook) {
  //block id
  $block_id = $vars['block']->delta;
  // Add a striping class.
  $vars['classes_array'][] = 'block-' . $vars['block_zebra'];

  // Add first/last block classes
  $first_last = "";
  // If block id (count) is 1, it's first in region.
  if ($vars['block_id'] == '1') {
    $first_last = "first";
    $vars['classes_array'][] = $first_last;
  }
  // Count amount of blocks about to be rendered in that region.
  $block_count = count(block_list($vars['elements']['#block']->region));
  if ($vars['block_id'] == $block_count) {
    $first_last = "last";
    $vars['classes_array'][] = $first_last;
  }

  switch ($block_id) {
    case 'my_preferences-block':
    case 'preferences_for_other-block':
      $date = getdate(time());
      $uid = arg(1);
      if ($uid) {
        if(!is_numeric($uid)){
          $bundle = arg(0);
          $uid = get_id_by_alias($bundle, $uid);
        }
      } else {
        $uid = false;
      }

      $data = _wsg_get_fitness_goals_data($uid, time());

      $data = $data[$date['year']][$date['month']];

      if (isset($data[0]['is_empty_current_month'] )) {
        $vars['classes_array'][] = 'without-goal';
      }

      break;
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function wellsquad_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('wellsquad_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('wellsquad_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('wellsquad_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('wellsquad_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $title = check_plain($item['title']);
        }
        else {
          $title = drupal_get_title();
        }
        if ($title) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('wellsquad_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users. Make the heading invisible with .element-invisible.
      $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

      return $heading . '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</div>';
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *  The string
 * @return
 *  The converted string
 */
function wellsquad_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $variables
 *  An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *  A themed HTML string.
 *
 * @ingroup themeable
 *
 */
function wellsquad_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // Contact item as popup
  if($element['#original_link']['menu_name'] == 'menu-footer-menu' && $element['#title'] == 'Contact') {
    $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal ctools-modal-modal-popup-small contact-us ';
  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = wellsquad_id_safe($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  if (isset($element['#original_link']['mlid']) && !empty($element['#original_link']['mlid'])) {
    $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  }
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override or insert variables into theme_menu_local_task().
 */
function wellsquad_preprocess_menu_local_task(&$variables) {
  $link =& $variables['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab">' . $link['title'] . '</span>';
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function wellsquad_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Implementation of hook_menu().
 */

function wellsquad_menu() {

    $items = array();

    //Not found page (404)
    $items['not-found'] = array(
        'title' => t('Page not found'),
        'page callback' => 'page_not_found_callback',
        'access callback' => TRUE,
    );

    return $items;
}

function page_not_found_callback(){
    return theme('not_found');
}

/**
 * theme_uc_cart_checkout_form()
 */
function wellsquad_uc_cart_checkout_form($variables) {

    return theme('wellsquad_uc_checkout_form', array('form' => $variables['form']));
}


/**
 * Formats the cart contents table on the checkout page.
 *
 * @param $variables
 *   An associative array containing:
 *   - show_subtotal: TRUE or FALSE indicating if you want a subtotal row
 *     displayed in the table.
 *   - items: An associative array of cart item information containing:
 *     - qty: Quantity in cart.
 *     - title: Item title.
 *     - price: Item price.
 *     - desc: Item description.
 *
 * @return
 *   The HTML output for the cart review table.
 *
 * @ingroup themeable
 */
function wellsquad_uc_cart_review_table($variables) {
    $items = $variables['items'];
    $data = array();

    // Get information about product
    foreach($items as $key => $item){
        $data['product_title'] = $item->title;

        // Get program length
        $field_length_of_time = field_get_items('node', $item, 'field_length_of_time');
        if(isset($field_length_of_time[0]['tid'])){
            $length_of_time = taxonomy_term_load($field_length_of_time[0]['tid']);
            $data['product_length'] = !empty($length_of_time) ? $length_of_time->name : '';
        }

        // Get program description
        $field_description = field_get_items('node', $item, 'field_description');
        if(!empty($field_description[0])){
            $data['product_description'] = !empty($field_description[0]['value']) ? $field_description[0]['value'] : '';
        }
    }

    return theme('wellsquad_uc_cart_checkout_pane', array('data' => $data));
}

/**
 * Implements hook_form_alter()
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function wellsquad_form_alter(&$form, $form_state, $form_id)  {

    switch ($form_id)  {
        case 'uc_cart_checkout_form':
            // change redirect after cancel checkout
            $form['panes']['cart']['#title'] = t('Your Order');

            if(isset($form['panes']['payment']['line_items'])){
                $line_items = $form['panes']['payment']['line_items'];
                unset($form['panes']['payment']['line_items']);
                $form['panes']['cart']['line_items'] = $line_items;
            }

            $form['actions']['cancel']['#submit'] = array('wellsquad_uc_cart_checkout_form_cancel');
            break;
    }

}

/**
 * Submit handler for "Cancel" button on uc_cart_checkout_form().
 *
 * @see uc_cart_checkout_form()
 */
function wellsquad_uc_cart_checkout_form_cancel($form, &$form_state) {
    global $base_url;
    $order = $form_state['storage']['order'];
    if (isset($_SESSION['cart_order']) && $_SESSION['cart_order'] == $order->order_id) {
        uc_order_comment_save($_SESSION['cart_order'], 0, t('Customer canceled this order from the checkout form.'));
        unset($_SESSION['cart_order']);
    }

    unset($_SESSION['uc_checkout'][$order->order_id]);

    if(isset($_COOKIE['Drupal_visitor_cancel_checkout_redirect']) && !empty($_COOKIE['Drupal_visitor_cancel_checkout_redirect'])){
        $url =  $base_url . '/' .$_COOKIE['Drupal_visitor_cancel_checkout_redirect'];
        $form_state['redirect'] = $url;
    }

}
/* End Ubercart customization  */

function _wellsquad_get_profile_title($s){

    switch($s){
        case 'profile-user_info_1':
        case 'profile-user_info_2':
        case 'profile-user_info_1_squad_leader':
        case 'profile-user_info_2_squad_leader':
            return t("Edit Profile");
            break;
        default:
            return $s;
    }

}

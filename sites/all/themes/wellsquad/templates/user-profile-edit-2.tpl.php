<?php
//Form parameters
print render($form['form_id']);
print render($form['form_build_id']);
print render($form['form_token']);

?>

<section class="profile-section profile-section--twocol border-bottom">
	<h4><?php print t('Fitness goals');?></h4>
	<div class="heading--addintion">
		<span><?php print _wellsquad_configuration_get_longterm_goal() . ' ' . t('with');?></span>
	</div>
	<div class="profile-box--centered2 profile-box--centered3">
		<!--//List of the goals (select inputs list)-->
		<?php echo render($form['profile_user_info_2']['field_fitness_goals']); ?>
		<!--END OF GOALS LIST-->
	</div>
</section>
<div class="profile-section profile-section--accent profile-section--twocol border-bottom">
	<div class="profile-box--centered2 profile-box--centered3">
		<?php echo render($form['profile_user_info_2']['field_motivation_level']);?>
		<?php echo render($form['profile_user_info_2']['field_method_of_tracking']);?>
	</div>
</div>
<section class="profile-section">
    <h4><?php print t('Used app`s');?></h4>
    <div class="profile-box--centered2 profile-box--centered3">
        <div class="col-container">

            <div class="brand">

                <div class="brand-item">
                    <?php echo render($form['profile_user_info_2']['field_nike_checkbox']); // NIKE+ CHECKBOX?>
                    <div class="brand-preview">
                        <img src="/<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-1.jpg" alt="Nike">
                    </div>
                    <div class="brand-holder">
                        <h3><?php print t('Nike+');?></h3>
                        <?php print l(t('Register'), variable_get('_nikeplus_registration_link'), array('attributes' => array('target'=>'_blank')));?>
                    </div>
                </div>

                <div class="brand-item">
                    <?php echo render($form['profile_user_info_2']['field_fitbit_checkbox']);// FITBIT CHECKBOX?>
                    <div class="brand-preview">
                        <img src="/<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-2.jpg" alt="Fitbit">
                    </div>
                    <div class="brand-holder">
                        <h3><?php print t('Fitbit');?></h3>
                        <?php print l( t('Register'), variable_get('_fitbit_registration_link'), array('attributes' => array('target'=>'_blank')));?>
                    </div>
                </div>

                <div class="brand-item">
                    <?php echo render($form['profile_user_info_2']['field_jawbone_checkbox']); // JAWBONE CHECKBOX?>
                    <div class="brand-preview">
                        <img src="/<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-3.jpg" alt="JawBone">
                    </div>
                    <div class="brand-holder">
                        <h3><?php print t('Jawbone');?></h3>
                        <?php print l(t('Register'), variable_get('_jawbone_regitration_link'), array('attributes' => array('target'=>'_blank')));?>
                    </div>
                </div>

                <div class="brand-item">
                    <?php echo render($form['profile_user_info_2']['field_foursquare']);// FOURSQUARE CHECKBOX?>
                    <div class="brand-preview">
                        <img src="/<?php echo drupal_get_path('theme', 'wellsquad');?>/images/brand-4.jpg" alt="FourSquare">
                    </div>
                    <div class="brand-holder">
                        <h3><?php print t('Foursquare');?></h3>
                        <?php print l(t('Register'), variable_get('_foursquare_registration_link'), array('attributes' => array('target'=>'_blank')));?>
                    </div>
                </div>

            </div>

        </div>
        <?php print render($form['actions']);?>
    </div>
</section>
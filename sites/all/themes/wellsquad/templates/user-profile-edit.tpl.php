<?php print render($form['form_id']); ?>
<?php print render($form['form_build_id']); ?>
<?php print render($form['form_token']); ?>

<section class="profile-section border-bottom">
	<h4><?php print t('Basic info'); ?></h4>
	<div class="profile-box--centered">
		<div class="profile-photo-wrap">
			<?php print render($form['profile_user_info_1']['field_profile_photo']); ?>
		</div>
		<div class="col-wrap">
			<div class="col-holder">
				<div class="profile-col">
					<?php print render($form['profile_user_info_1']['field_first_name']); ?>
					<?php print render($form['profile_user_info_1']['field_last_name']); ?>
					<?php print render($form['profile_user_info_1']['field_body_type']); ?>
					<?php print render($form['profile_user_info_1']['field_relationship_status']); ?>
				</div>
				<div class="profile-col2">
					<?php print render($form['profile_user_info_1']['field_male']); ?>
					<?php print render($form['profile_user_info_1']['field_zip_code']); ?>
					<?php print render($form['profile_user_info_1']['field_age_range']); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="profile-section">
    <h4><?php print t('Preferences'); ?></h4>
	<div class="profile-box--centered2">
		<div class="col-container">
			<?php print render($form['profile_user_info_1']['field_partner_male']); ?>
			<?php print render($form['profile_user_info_1']['field_prefer_workout_partner']); ?>
			<?php print render($form['profile_user_info_1']['field_my_schedule']); ?>
			<?php print render($form['profile_user_info_1']['field_prefer_workout_in']); ?>
			<?php print render($form['profile_user_info_1']['field_consider_myself']); ?>
			<?php print render($form['profile_user_info_1']['field_partner_to_be']); ?>
			<?php print render($form['profile_user_info_1']['field_fitness_experience']); ?>
			<?php print render($form['profile_user_info_1']['field_partners_experience']); ?>
			<?php print render($form['profile_user_info_1']['field_preferred_workout_location']); ?>
			<?php print render($form['profile_user_info_1']['field_longterm_goal']); ?>
		</div>
		<div class="col-container2">
			<?php print render($form['profile_user_info_1']['field_exercise_favourite_type']); ?>
		</div>
        <div class="profile-dietitian">
            <?php print render($form['profile_user_info_1']['field_nutrition_services']); ?>
        </div>
        <?php print render($form['actions']); ?>
	</div>
</section>
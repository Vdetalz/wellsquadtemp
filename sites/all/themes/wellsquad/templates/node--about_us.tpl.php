<?php /** Template for about us page */ ?>

<?php if($items = field_get_items('node', $node, 'field_founders')): ?>
    <div class="founders">
        <?php foreach($items as $item): ?>
            <?php if($fc_items = field_collection_field_get_entity($item)): ?>
                <div class="founder-holder">
                    <div class="founder">
                        <?php if($field_founder_photo = field_get_items('field_collection_item', $fc_items, 'field_founder_photo')): ?>
                            <div class="founder-photo">
                                <img src="<?php print image_style_url('founder_photo', $field_founder_photo[0]['uri']); ?>" alt="<?php print $field_founder_photo[0]['filename']; ?>">
                            </div>
                        <?php endif; ?>
                        <article class="founder-desc">
                        <?php if($field_founder_name = field_get_items('field_collection_item', $fc_items, 'field_founder_name')): ?>
                            <h2 class="accent"><?php print $field_founder_name[0]['value']; ?></h2>
                        <?php endif; ?>

                        <?php if($field_founder_description = field_get_items('field_collection_item', $fc_items, 'field_founder_description')): ?>
                                <?php print $field_founder_description[0]['value']; ?>
                        <?php endif; ?>
                        </article>

                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="btn-wrap btn-wrap--contact">
<?php // Contact us popup
print l(t('CONTACT US'), 'modal_forms/nojs/webform/9', array('attributes' => array('class' => 'ctools-use-modal ctools-modal-modal-popup-small contact-us')));
?>
</div>

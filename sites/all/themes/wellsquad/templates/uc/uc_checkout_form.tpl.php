<?php
/**
 * Template for checkout form
 */
?>

<div class="payment-methods">
    <?php print render($form['panes']['billing']);?>
</div>
<div class="payment-methods">
    <?php print render($form['panes']['payment']);?>
</div>
<div class="your-order">
    <?php print render($form['panes']['cart']['cart_review_table']);?> 
    <?php print render($form['panes']['cart']['line_items']);?>
</div>
<div class="submit-holder">
<?php  print drupal_render_children($form);?>
</div>

<?php
/**
 * Template for cart content on the checkout page
 */
?>
<?php if (!empty($data)):?>
    <div class="product-info">
        <legend><span class="fieldset-legend"><?php print t('Your Order'); ?></span></legend>
        <p><strong class="title"><?php print $data['product_title'];?></strong></p>
        <?php if(!empty($data['product_length'])):?>
             <p><?php print t('Length of time') . ': '?><span class="param"><?php print $data['product_length']; ?></span></p>
        <?php endif;?>
        <?php if(!empty($data['product_description'])):?>
             <p><?php print t('Description') . ': '?><span class="param"><?php print $data['product_description']; ?></span></p>
        <?php endif;?>
    </div>
<?php endif;?>
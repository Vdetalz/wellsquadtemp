<section id="content" class="content not-found">
    <div id="content-area">
        <?php print t('The requested page could not be found.'); ?>
    </div>
</section>

<?php
global $user;
$alias = arg(1);
$bundle = arg(0);

if(!is_numeric($alias)){
    $uid = get_id_by_alias($bundle, $alias);
}else{
    $uid = $alias;
}

$b = user_load($uid);
$c = user_relationships_ui_actions_between($user, $b, array('add' => 1, 'requested' => 1, 'received' => 1));

if (!empty($c[0])) {
        $match_link = preg_replace("/user_relationships_popup_link/", 'user_relationships_popup_link btn', $c[0]);
		print $match_link;
}

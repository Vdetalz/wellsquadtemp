<?php /** Template for FAQ page */ ?>

<?php if($items = field_get_items('node', $node, 'field_questions')): ?>
    <div class="questions accordion">
        <?php foreach($items as $item): ?>
            <?php if($fc_items = field_collection_field_get_entity($item)): ?>
                <article class="faq">
                    <?php if($field_question = field_get_items('field_collection_item', $fc_items, 'field_question')): ?>
                        <h3 class="qustion opener"><?php print $field_question[0]['value']; ?></h3>
                    <?php endif; ?>

                    <?php if($field_answer = field_get_items('field_collection_item', $fc_items, 'field_answer')): ?>
                        <div class="answer slide">
                            <?php print $field_answer[0]['value']; ?>
                        </div>
                    <?php endif; ?>
                </article>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
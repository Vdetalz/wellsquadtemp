<?php
print render($form['form_id']);
print render($form['form_build_id']);
print render($form['form_token']);
?>
<section class="profile-section border-bottom">
	<div class="profile-box--centered">
		<div class="profile-photo-wrap">
			<?php echo render($form['profile_user_info_1_squad_leader']['field_profile_photo']); ?>
		</div>
		<div class="col-wrap">
			<div class="col-holder">
				<div class="profile-col">
					<?php echo render($form['profile_user_info_1_squad_leader']['field_first_name']); ?>
					<?php echo render($form['profile_user_info_1_squad_leader']['field_last_name']); ?>
					<?php echo render($form['profile_user_info_1_squad_leader']['field_phone_number']); ?>
				</div>
				<div class="profile-col2">
					<?php echo render($form['profile_user_info_1_squad_leader']['field_male']); ?>
					<?php echo render($form['profile_user_info_1_squad_leader']['field_zip_code']); ?>
					<?php echo render($form['profile_user_info_1_squad_leader']['field_age_range']); ?>
				</div>
			</div>

			<?php echo render($form['profile_user_info_1_squad_leader']['field_tagline']); ?>
			<?php echo render($form['profile_user_info_1_squad_leader']['field_url_http_']); ?>
		</div>
	</div>
</section>
<section class="profile-section">
    <h4><?php print t("Experience info &amp; Program Details"); ?></h4>
	<div class="profile-box--centered2">
		<div class="col-container">
			<?php echo render($form['profile_user_info_1_squad_leader']['field_kind_of_professional']); ?>
			<?php echo render($form['profile_user_info_1_squad_leader']['field_fitness_experience_int']); ?>
            <?php echo render($form['profile_user_info_1_squad_leader']['field_certification_file']); ?>
		</div>
		<div class="col-container2">
			<?php echo render($form['profile_user_info_1_squad_leader']['field_exercise_favourite_type']); ?>
		</div>
		<?php print render($form['actions']);?>
	</div>
</section>
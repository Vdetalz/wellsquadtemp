<?php global $user;?>

<div class="invite">
	<h3><?php print($form['#node']->title);?></h3>
	<?php
		print render($form['form_id']);
		print render($form['form_build_id']);
		print render($form['form_token']);
		$form['actions']['submit']['#value'] = t('SEND');
	?>
	<div class="form-row2">
		<label for=""><?php print t('From:');?></label>
		<span class="param-name"><?php print $user->mail;?></span>
	</div>
	<?php print drupal_render($form['submitted']['recipient_email']);?>
	<div class="form-row2">
		<label for=""><?php print t('Subject:');?></label>
		<?php print drupal_render($form['submitted']['subject']);?>
	</div>
	<?php print drupal_render($form['submitted']['message']);?>
	<?php print render($form['actions']);?>
    <div id="fb-root"></div>
	<a href="#"  class='facebook-invite-link'><?php print t("Invite Friends From Facebook");?></a>

</div>
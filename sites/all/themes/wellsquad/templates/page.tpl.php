<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->
  <div class="main-wrap">
  <header id="header" class="header <?php if(isset($header_class)) print $header_class; ?>">
    <div class="container">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>

      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">

          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </div>
      <?php endif; ?>

      <?php if ($page['header']): ?>
        <div id="header-region" class="header-region">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
    </div>
  </header> <!-- /header -->

    <?php if ($page['before_content']): ?>
        <div id="before-content" class="before-content">
            <?php print render($page['before_content']); ?>
        </div>
    <?php endif; ?>

  <?php if ($main_menu || $secondary_menu): ?>
    <nav id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";}
      if (!empty($secondary_menu)) {print " with-secondary";} ?>">
      <div class="container">
        <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
        <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
      </div>
    </nav> <!-- /navigation -->
  <?php endif; ?>

  <!-- ______________________ MAIN _______________________ -->
  <div id="main">
    <div class="container">
      <?php if ($page['sidebar_first']): ?>
        <aside id="sidebar-first" class="column sidebar first border-right">
          <?php print render($page['sidebar_first']); ?>
          <a href="#" class="close-btn"></a>
        </aside>
      <?php endif; ?> <!-- /sidebar-first -->
      <section id="content" class="content">
          <?php if ($page['sidebar_first']): ?>
              <a href="#" class="open-btn">open menu</a>
          <?php endif; ?>
          <?php if ($breadcrumb || $title|| $messages || $action_links || (in_array(current_path(), array('squads', 'connections'))) || $html_title): ?>
            <div id="content-header">

              <?php print $breadcrumb; ?>

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>
                  <?php if ($title || (in_array(current_path(), array('squads', 'connections'))) || $html_title): ?>
                <h1 class="title heading--styled">
                    <?php
                    if(((isset($node)) || (in_array(current_path(), array('squads', 'connections')))) && user_is_logged_in()):?>
                        <?php echo l(t('INVITE'), 'modal_forms/nojs/webform/141', array('attributes' =>
                            array('class' => 'ctools-use-modal ctools-modal-modal-popup-small contact-us')));?>
                        <span><?php echo t('Friends to');?></span>
                        <?php echo t('wellsquad');?>
                    <?php elseif( stristr(arg(0), 'profile-user') ):?>
                        <?php print _wellsquad_get_profile_title(arg(0)); ?>
                    <?php else:?>
                        <?php print $title; ?>
                    <?php endif;?>
                    <?php
                        if($html_title){
                            print $html_title;
                        }
                    ?>
                </h1>
              <?php endif; ?>
              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>

              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>
		  <?php print render($page['search_bar']); ?>
         <?php if ($tabs && user_access('administer')): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
         <?php endif; ?>

          <div id="content-area">
            <?php print render($page['content']) ?>
          </div>

          <?php print $feed_icons; ?>

      </section> <!-- /content-inner /content -->

      <?php if ($page['sidebar_second']): ?>
        <aside id="sidebar-second" class="column sidebar second">
          <?php print render($page['sidebar_second']); ?>
        </aside>
      <?php endif; ?> <!-- /sidebar-second -->
    </div>
  </div> <!-- /main -->

  <!-- ______________________ FOOTER _______________________ -->
  </div>
  <?php if ($page['footer']): ?>
    <footer id="footer" class="footer">
      <div class="footer-wrap">
        <div class="container">
        <?php print render($page['footer']); ?>
        </div>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->

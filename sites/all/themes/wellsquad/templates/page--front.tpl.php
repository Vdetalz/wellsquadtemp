<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->
  <div class="main-wrap">
    <header id="header" class="header header--transparent">
      <div class="container">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
          </a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan">

            <?php if ($site_name): ?>
              <?php if ($title): ?>
                <div id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
                </div>
              <?php else: /* Use h1 when the content title is empty */ ?>
                <h1 id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
                </h1>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>

          </div>
        <?php endif; ?>

        <?php if ($page['header']): ?>
          <div id="header-region" class="header-region">
            <?php print render($page['header']); ?>
          </div>
        <?php endif; ?>
      </div>
    </header> <!-- /header -->

    <?php if ($main_menu || $secondary_menu): ?>
      <nav id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";}
      if (!empty($secondary_menu)) {print " with-secondary";} ?>">
        <div class="container">
          <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
          <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
        </div>
      </nav> <!-- /navigation -->
    <?php endif; ?>

    <!-- ______________________ MAIN _______________________ -->

    <div id="main" role="main" class="main">
      <div class="container">
        <section id="content">

          <?php if( count($node->field_promotion_data) ):
            foreach( $node->field_promotion_data as $item ):?>

              <div class="head-section">
                <?php if($item['background-image'][0]['uri']):?>
                  <div class="preview-holder">

                    <img src="<?php echo file_create_url( $item['background-image'][0]['uri'] );?>" alt="preview"/>
                  </div>
                <?php endif;?>
                <div class="description-content">
                  <?php echo $item['content'][0]['value'];?>
                </div>
              </div>

            <?php endforeach;?>
          <?php endif;?>

        </section> <!-- /content-inner /content -->
      </div>
    </div> <!-- /main -->
  </div>

  <!-- ______________________ BOTTOM REGINON _______________________ -->
  <?php if ($page['bottom']): ?>
    <div class="bottom">
      <div class="bottom-wrap">
        <div class="container">
          <?php print render($page['bottom']); ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
  <!-- ______________________ FOOTER _______________________ -->

  <?php if ($page['footer']): ?>
    <footer id="footer" class="footer">
      <div class="footer-wrap">
        <div class="container">
          <?php print render($page['footer']); ?>
        </div>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /page -->

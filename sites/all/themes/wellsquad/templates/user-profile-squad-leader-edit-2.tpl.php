<?php
print render($form['form_id']);
print render($form['form_build_id']);
print render($form['form_token']);
?>

<section class="profile-section border-bottom">
	<h4><?php print t('Experience &amp; Program details'); ?></h4>
	<div class="profile-box--centered2 profile-box--centered4">
		<div class="col-container">
			<div class="form-item form-item--textarea">
				<?php echo render($form['profile_user_info_2_squad_leader']['field_accomplishments']); ?>
			</div>
			<div class="form-item">
				<?php echo render($form['profile_user_info_2_squad_leader']['field_classes_per_month']); ?>
			</div>
			<div class="form-item">
				<?php echo render($form['profile_user_info_2_squad_leader']['field_users_per_class']); ?>
			</div>
		</div>
	</div>
</section>
<section class="profile-section profile-section--noheading">
	<div class="heading--addintion">
		<span><?php print t('References'); ?></span>
	</div>
	<div class="profile-box--centered2 profile-box--centered4">
		<div class="col-container col-container--triple">
			<p><?php print t('Please enter 2 references that can confirm your accomplishments:'); ?></p>
				<div class="form-row">
					<span class="param-head"><?php print t('Reference #1'); ?></span>
					<div class="row-box">
						<?php echo render($form['profile_user_info_2_squad_leader']['field_reference_1_name']); ?>
						<?php echo render($form['profile_user_info_2_squad_leader']['field_reference_1_email']); ?>
					</div>
				</div>
				<div class="form-row">
					<span class="param-head"><?php print t('Reference #2'); ?></span>
					<div class="row-box">
						<?php echo render($form['profile_user_info_2_squad_leader']['field_reference_2_name']); ?>
						<?php echo render($form['profile_user_info_2_squad_leader']['field_reference_2_email']); ?>
					</div>
				</div>
		</div>
		<?php print render($form['actions']);?>
	</div>
</section>
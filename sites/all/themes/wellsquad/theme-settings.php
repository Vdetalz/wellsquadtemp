<?php

// Form override for theme settings
function wellsquad_form_system_theme_settings_alter(&$form, $form_state) {
  $form['options_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Specific Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );
  $form['options_settings']['wellsquad_tabs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the ZEN tabs'),
    '#description' => t('Check this if you wish to replace the default tabs by the ZEN tabs'),
    '#default_value' => theme_get_setting('wellsquad_tabs'),
  );
  $form['options_settings']['wellsquad_breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb settings'),
    '#attributes' => array('id' => 'basic-breadcrumb'),
  );
  $form['options_settings']['wellsquad_breadcrumb']['wellsquad_breadcrumb'] = array(
    '#type' => 'select',
    '#title' => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('wellsquad_breadcrumb'),
    '#options' => array(
      'yes' => t('Yes'),
      'admin' => t('Only in admin section'),
      'no' => t('No'),
    ),
  );
  $form['options_settings']['wellsquad_breadcrumb']['wellsquad_breadcrumb_separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Breadcrumb separator'),
    '#description' => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => theme_get_setting('wellsquad_breadcrumb_separator'),
    '#size' => 5,
    '#maxlength' => 10,
    '#prefix' => '<div id="div-basic-breadcrumb-collapse">', // jquery hook to show/hide optional widgets
  );
  $form['options_settings']['wellsquad_breadcrumb']['wellsquad_breadcrumb_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show home page link in breadcrumb'),
    '#default_value' => theme_get_setting('wellsquad_breadcrumb_home'),
  );
  $form['options_settings']['wellsquad_breadcrumb']['wellsquad_breadcrumb_trailing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('wellsquad_breadcrumb_trailing'),
    '#description' => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['options_settings']['wellsquad_breadcrumb']['wellsquad_breadcrumb_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('wellsquad_breadcrumb_title'),
    '#description' => t('Useful when the breadcrumb is not placed just before the title.'),
    '#suffix' => '</div>', // #div-basic-breadcrumb-collapse
  );

  //IE specific settings.
  $form['options_settings']['wellsquad_ie'] = array(
    '#type' => 'fieldset',
    '#title' => t('Internet Explorer Stylesheets'),
    '#attributes' => array('id' => 'basic-ie'),
  );
  $form['options_settings']['wellsquad_ie']['wellsquad_ie_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Internet Explorer stylesheets in theme'),
    '#default_value' => theme_get_setting('wellsquad_ie_enabled'),
    '#description' => t('If you check this box you can choose which IE stylesheets in theme get rendered on display.'),
  );
  $form['options_settings']['wellsquad_ie']['wellsquad_ie_enabled_css'] = array(
    '#type' => 'fieldset',
    '#title' => t('Check which IE versions you want to enable additional .css stylesheets for.'),
    '#states' => array(
      'visible' => array(
        ':input[name="wellsquad_ie_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['options_settings']['wellsquad_ie']['wellsquad_ie_enabled_css']['wellsquad_ie_enabled_versions'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'ie8' => t('Internet Explorer 8'),
      'ie9' => t('Internet Explorer 9'),
      'ie10' => t('Internet Explorer 10'),
    ),
    '#default_value' => theme_get_setting('wellsquad_ie_enabled_versions'),
  );

  $form['options_settings']['clear_registry'] = array(
     '#type' => 'checkbox',
     '#title' => t('Rebuild theme registry on every page.'),
     '#description' =>t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
     '#default_value' => theme_get_setting('clear_registry'),
  );
  $form['logo']['settings']['sublogo'] = array(
     '#type' => 'fieldset',
     '#title' => t('Second logo'),
     '#description' => t("Second logo for your page")
  );

  $logo_path = theme_get_setting('sublogo_path');
  $second_logo_path = '';
  // If $logo_path is a public:// URI, display the path relative to the files
 // directory; stream wrappers are not end-user friendly.
  if (file_uri_scheme($logo_path) == 'public') {
      $logo_path = file_uri_target($logo_path);
      if(!empty($logo_path)){
          $second_logo_path = $logo_path;
      }
  }

  $form['logo']['settings']['sublogo']['sublogo_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to second logo'),
      '#default_value' =>  $second_logo_path,
  );
  $form['logo']['settings']['sublogo']['sublogo_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload second logo'),
  );
  $form['#submit'][]   = 'wellsquad_settings_submit';
}


function wellsquad_settings_submit($form, &$form_state) {
    $settings = array();

    // Check for a new uploaded file, and use that if available.
    if ($file = file_save_upload('sublogo_upload')) {
        $parts = pathinfo($file->filename);
        $destination = 'public://' . $parts['basename'];
        $file->status = FILE_STATUS_PERMANENT;


        if (file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
            $_POST['sublogo_path'] = $form_state['values']['sublogo_path'] = $destination;
        }
    }
}
